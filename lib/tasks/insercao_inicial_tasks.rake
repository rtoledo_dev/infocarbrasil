namespace :insercao_inicial do
  desc 'Inserir dados iniciais'
  task :opcionais_tipos_veiculos do
    require "config/environment"
    
    
    TipoVeiculo.all.collect do |t|
      t.opcionais = Opcional.all
      t.save
    end
  end
end

