﻿/**
 * $Id: editor_plugin_src.js 201 2007-02-12 15:56:56Z spocke $
 *
 * @author Moxiecode
 * @copyright Copyright © 2004-2008, Moxiecode Systems AB, All rights reserved.
 */

(function() {
    // Load plugin specific language pack
    tinymce.PluginManager.requireLangPack('cmsAgence');

    tinymce.create('tinymce.plugins.CmsAgencePlugin', {
        /**
         * Initializes the plugin, this will be executed after the plugin has been created.
         * This call is done before the editor instance has finished it's initialization so use the onInit event
         * of the editor instance to intercept that event.
         *
         * @param {tinymce.Editor} ed Editor instance that the plugin is initialized in.
         * @param {string} url Absolute URL to where the plugin is located.
         */
        init : function(ed, url) {
            var componentes_enabled, cms_agence_site_id = ed.getParam('cms_agence_site_id');

            new Ajax.Request('/admin/conteudo_geral/getTodosComponentes',
            { asynchronous:false,
                evalScripts:true,
                onComplete: function(request){
                    componentes_enabled = request.responseText.evalJSON();
                },
                parameters: 'estado_id=' + this.value
            }
        );

            // Register the command so that it can be invoked by using tinyMCE.activeEditor.execCommand('mceCmsAgence');
            ed.addCommand('mceCmsAgence', function() {
                var componente_ativo = '', nodeName = ed.selection.getNode().nodeName.toLowerCase().replace('cms:', '');

                for (var i = 0; i < componentes_enabled.length; i++)
                {
                    if (componentes_enabled[i][0] == nodeName)
                    {
                        componente_ativo = nodeName;
                    }
                }

                ed.windowManager.open({
                    file : "/admin/inserir_componente?site_id=" + cms_agence_site_id + "&tipo=" + componente_ativo,
                    width : 370 + parseInt(ed.getLang('cmsAgence.delta_width', 0)),
                    height : 250 + parseInt(ed.getLang('cmsAgence.delta_height', 0))
                }, {
                    plugin_url : url
                });
            });

            // Register cmsAgence button
            ed.addButton('cmsAgence', {
                title : 'cmsAgence.desc',
                cmd : 'mceCmsAgence',
                image : url + '/img/cmsAgence.gif'
            });

            // Add a node change handler, selects the button in the UI when a image is selected
            ed.onNodeChange.add(function(ed, cm, n) {
                var nodeName = n.nodeName.toLowerCase().replace('cms:', '');
                var isComponente = false;

                for (var i = 0; i < componentes_enabled.length; i++)
                {
                    if (componentes_enabled[i][0] == nodeName) isComponente = true;
                }
                
                cm.setActive('cmsAgence', (isComponente));
            });
        },

        /**
         * Creates control instances based in the incomming name. This method is normally not
         * needed since the addButton method of the tinymce.Editor class is a more easy way of adding buttons
         * but you sometimes need to create more complex controls like listboxes, split buttons etc then this
         * method can be used to create those.
         *
         * @param {String} n Name of the control to create.
         * @param {tinymce.ControlManager} cm Control manager to use inorder to create new control.
         * @return {tinymce.ui.Control} New control instance or null if no control was created.
         */
        createControl : function(n, cm) {
            return null;
        },

        /**
         * Returns information about the plugin as a name/value array.
         * The current keys are longname, author, authorurl, infourl and version.
         *
         * @return {Object} Name/value array containing information about the plugin.
         */
        getInfo : function() {
            return {
                longname : 'CmsAgence plugin',
                author : 'Raphael A. Araújo',
                authorurl : 'mailto:raphael.araujo@agence.com.br',
                infourl : 'http://www.agence.com.br',
                version : "1.0"
            };
        }
    });

    // Register plugin
    tinymce.PluginManager.add('cmsAgence', tinymce.plugins.CmsAgencePlugin);
})();