// Coloca tabelas com linhas de cores intercaladas
function linesTrue(idTable)
{
    tabelas = document.getElementsByClassName('relatorio');
		
    for (var j = 0; j < tabelas.length; j++)
    {
        var linhas = tabelas[j].getElementsByTagName('tbody')
        if(linhas.length)
        {
            var todasLinhas = linhas[0].getElementsByTagName('tr');
			
            for (var i = 0; i < todasLinhas.length; i++ )
            {
                if (i % 2 == 0)
                    todasLinhas[i].className = 'cor';
            }
        }
    }
}

// Passando o elemento da lista que ser� ativado, a fun��o a ativa e desativa as demais.
function activeTab(tab)
{
    lista = tab.parentNode.childNodes;
	
    for (var i = 0; i < lista.length; i++)
    {
        if (lista[i].innerHTML && lista[i] == tab)
        {
            if (lista[i].className.search('active') == -1)
                lista[i].className += ' active';
        }
        else if (lista[i].innerHTML)
        {
            lista[i].className = lista[i].className.replace('active','');
        }
    }
}

// ativa aba da listagem de abas global de acordo com o id
function activeTabById(tabId)
{
    lista = document.getElementById('tabs').getElementsByTagName('ul')[0].childNodes;

    for (var i = 0; i < lista.length; i++)
    {
        if (lista[i].id == tabId)
        {
            activeTab(lista[i]);
        }
    }
}

// exibe aba da listagem de abas global de acordo com o id
function showTabById(tabId)
{
    lista = document.getElementById('tabs').getElementsByTagName('ul')[0].childNodes;

    for (var i = 0; i < lista.length; i++)
    {
        if (lista[i].id == tabId)
        {
            lista[i].style.display = 'inline';
        }
    }
}

// esconde aba da listagem de abas global de acordo com o id
function hideTabById(tabId)
{
    lista = document.getElementById('tabs').getElementsByTagName('ul')[0].childNodes;

    for (var i = 0; i < lista.length; i++)
    {
        if (lista[i].id == tabId)
        {
            lista[i].style.display = 'none';
        }
    }
}

function exibirLoading()
{
    var strDivId = 'divLoading';
    var iebody=(document.compatMode && document.compatMode != "BackCompat")? document.documentElement : document.body;
	
    objDiv = document.getElementById(strDivId);
	
    var dsocleft=document.all? iebody.scrollLeft : pageXOffset;
    var dsoctop=document.all? iebody.scrollTop : pageYOffset;
	
    if (document.all||document.getElementById)
    {
        objDiv.style.left=dsocleft+2+"px";
        objDiv.style.top=dsoctop+2+"px";	
    }	
	
    $(strDivId).show();
}

function ocultarLoading()
{
    var strDivId = 'divLoading';
    $(strDivId).hide();
}

function teste_envio(url)
{
    if(url != "")
    {
        window.open('/admin/enviar_email/teste_envio?url=' + url,'teste_envio','height=310,width=574,scrollbars=yes'); " href='/admin/enviar_email/teste_envio'";
    }
    else
    {
        window.alert('Entre com a URL!');
    }	
}