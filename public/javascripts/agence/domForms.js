// Aplica click em todas as tags 'buttom' da pagina atual
function buttonActive()
{
    var botoes = document.getElementsByTagName('button');
    for (var i = 0; i < botoes.length; i++ )
    {
        botoes[i].onmousedown = function(){ this.className = 'click'; }
        botoes[i].onmouseup = function(){ this.className = ''; }
        botoes[i].onmouseout = function(){ this.className = ''; }
    }
}

// Aplica classe ao campo focado
function onFocusInput(campo)
{
    campo.className += ' inputFocus';
}

// Retira classe de focado ao campo desfocado
function onBlurInput(campo)
{
    campo.className = campo.className.replace('inputFocus', '');
}

// Aplica a todos os campos dos forms da pagina atual
function activeForm()
{
    var forms = document.getElementsByTagName('form');
    for (var j = 0; j < forms.length; j++)
    {
        for (var i = 0; i < forms[j].length; i++)
        {
            campo = forms[j][i];
            if ((campo.type == 'text' || campo.type == 'textarea' || campo.type == 'selectarea' || campo.type == 'password') && (!campo.onfocus && !campo.onblur))
            {
                campo.onfocus	= function () { onFocusInput( this ); };
                campo.onblur	= function () { onBlurInput( this ); };
            }
        }
    }
}

// Limpa a todos os campos do form passado como argumento
function limpaForm(form)
{
    for (var i = 0; i < form.length; i++)
    {
        campo = form[i];
        if (campo.type == 'file' || campo.type == 'text' || campo.type == 'textarea' || campo.type == 'selectarea' || campo.type == 'password' || campo.type == 'hidden')
        {
            campo.value = '';
        }
        else if (campo.type == 'textarea')
        {
            campo.selectedIndex = 0;
        }
        else if (campo.type == 'checkbox')
        {
            campo.checked = false;
        }
    }
}

// Popular um elemento do tipo select baseado em um objeto JSON
function mountSelectByJSON(element_id, jsonString)
{
    var element = $(element_id);
    var jsonObject = eval('(' + jsonString + ')');
	
    if (jsonObject.length && element)
    {			
        element.length = jsonObject.length;
		
        for (var i = 0; i < jsonObject.length; i++)
        {
            element.options[i].value = jsonObject[i][1];
            element.options[i].text = jsonObject[i][0];
        }
    }
    else if (element)
    {
        element.length = 1;
        element.options[0].value = '';
        element.options[0].text = "Nenhum registro encontrado";
    }
    else
    {
        alert("O sistema se comportou de forma inesperada!");
    }
}

// Popular um elemento do tipo select baseado em um objeto JSON
function mountSelectByJSONPlus(element, jsonObject)
{
    if (jsonObject.length && element)
    {			
        element.length = jsonObject.length;
		
        for (var i = 0; i < jsonObject.length; i++)
        {
            element.options[i].value = jsonObject[i][1];
            element.options[i].text = jsonObject[i][0];
        }
    }
    else if (element)
    {
        element.length = 1;
        element.options[0].value = '';
        element.options[0].text = "Nenhum registro encontrado";
    }
    else
    {
        alert("O sistema se comportou de forma inesperada!");
    }
}