function myFileBrowser (field_name, url, type, win) {
    // alert("Field_Name: " + field_name + "\nURL: " + url + "\nType: " + type + "\nWin: " + win);

    tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Pesquisar imagens',
        width : 500,  // Your dimensions may differ - toy around with them!
        height : 400,
        scrollbars : "yes",
        resizable : "yes",
        inline : "yes",
        content_css : "no",
        close_previous : "no"
    }, {
        window : win,
        input : field_name
    });
    return false;
}