// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
function hideAllBoxes(){
  var boxes = document.getElementsByClassName("box_info");
  for (var i = 0; i < boxes.length; i+= 1){
    box = boxes[i];
    box.style.display = 'none'
  }
}

function setarBotaoAoSalvar(obj)
{
    obj.form.botao_clicado.value = obj.name;
}

//Função para mostrar/esconder imagem no cadastro de perguntas
function muda_correta(){
	if(document.getElementById('pergunta_correta_1').checked){
		document.getElementById('icon_correta_1').style.visibility=''
		document.getElementById('icon_correta_2').style.visibility='hidden'
		document.getElementById('icon_correta_3').style.visibility='hidden'
		document.getElementById('icon_correta_4').style.visibility='hidden'
		document.getElementById('icon_correta_5').style.visibility='hidden'
	}
	if(document.getElementById('pergunta_correta_2').checked){
		document.getElementById('icon_correta_2').style.visibility=''
		document.getElementById('icon_correta_1').style.visibility='hidden'
		document.getElementById('icon_correta_3').style.visibility='hidden'
		document.getElementById('icon_correta_4').style.visibility='hidden'
		document.getElementById('icon_correta_5').style.visibility='hidden'
	}
	if(document.getElementById('pergunta_correta_3').checked){
		document.getElementById('icon_correta_3').style.visibility=''
		document.getElementById('icon_correta_1').style.visibility='hidden'
		document.getElementById('icon_correta_2').style.visibility='hidden'
		document.getElementById('icon_correta_4').style.visibility='hidden'
		document.getElementById('icon_correta_5').style.visibility='hidden'
	}
	if(document.getElementById('pergunta_correta_4').checked){
		document.getElementById('icon_correta_4').style.visibility=''
		document.getElementById('icon_correta_1').style.visibility='hidden'
		document.getElementById('icon_correta_2').style.visibility='hidden'
		document.getElementById('icon_correta_3').style.visibility='hidden'
		document.getElementById('icon_correta_5').style.visibility='hidden'
	}
	if(document.getElementById('pergunta_correta_5').checked){
		document.getElementById('icon_correta_5').style.visibility=''
		document.getElementById('icon_correta_1').style.visibility='hidden'
		document.getElementById('icon_correta_2').style.visibility='hidden'
		document.getElementById('icon_correta_3').style.visibility='hidden'
		document.getElementById('icon_correta_4').style.visibility='hidden'
	}
		
}

//Função para o cronômetro do quiz
// Variáveis globais
horaEnt= new Date()
paraContagem = false
var mins=0;
var segs=0;
var timer;
var tempo_total = -1;

//Funções do Cronômetro
function cronometro(segundos) {
	var quiz_minutos =  (segundos/60 >= 1)? Math.floor(segundos/60) : 0 
	var quiz_segundos = (segundos-quiz_minutos*60)
	mins = quiz_minutos;
	segs = quiz_segundos;
	entrada();
	cronoStart();
}
function cronoStart() {
	//decrementa minuto
	if(segs < 0) {
		mins--;
		segs=59;
	}
	
	var formSegs=(segs < 10) ? "0"+segs : (mins<0 && segs==59)?'00':segs;
	var formMins=(mins < 10) ? (mins<0)?"00":"0"+mins : mins;
	
	if(!paraContagem){
		document.getElementById('crono').value=formMins+"M"+formSegs+"s","crono";
		tempo_total++;
	}
	
	//verifica se tempo acabou
	if(mins<=0 && segs==0){
		saida();
		document.form_quiz.submit();
	}
	
	segs--;
	timer=window.setTimeout("cronoStart();",1000);
}

//Função para pegar tempo de entrada e saída
function entrada(){
	horaEnt=new Date()
}
function saida(){
	document.form_quiz.tempo.value = tempo_total;
	paraContagem = true
	window.alert("Você terminou o quiz em "+tempo_total+" segundos!\n Obrigado por participar!")
}
function remover_eleitor(){
	
	document.getElementById("acao").value = "remover"
	
	document.getElementById('form_usuarios').onsubmit();
}
function adicionar_eleitor(){
	document.getElementById("acao").value = "adicionar"
	
	document.getElementById('form_usuarios').onsubmit();

}
function remover_elegivel(){
	
	document.getElementById("acao").value = "remover"
	
	document.getElementById('form_usuarios').onsubmit();
}
function adicionar_elegivel(){
	document.getElementById("acao").value = "adicionar"
	
	document.getElementById('form_usuarios').onsubmit();

}

function loading_portlet(portlet)
{
    var height = portlet.getHeight();
    var message = '<div class="portlet_loading" style="heigth:300px"><img src="/images/portlet_loading.gif" /></div>';
    
    portlet.style.height = (height - 20) + 'px';
    portlet.innerHTML = message;
}

function changePagePortlet(element, componente_name, page, parameters)
{
    portlet = $(element).up(1);
    new Ajax.Updater(portlet, '/campanha/cms_componente/' + componente_name +'?page=' + page, {asynchronous:true, evalScripts:true, onFailure:function(request){alert('Ocorreu um erro na operação, tente novamente mais tarde ou entre em contato pelo Fale Conosco');}, onLoading: loading_portlet(portlet), parameters: parameters});
    return false;
}

function remover_elegivel(){
	
	document.getElementById("acao").value = "remover"
	
	document.getElementById('form_usuarios').onsubmit();
}
function adicionar_elegivel(){
	document.getElementById("acao").value = "adicionar"
	
	document.getElementById('form_usuarios').onsubmit();

}

function remover_eleitor(){
	
	document.getElementById("acao_eleitor").value = "remover"
	
	document.getElementById('form_usuarios_eleitor').onsubmit();
}
function adicionar_eleitor(){
	document.getElementById("acao_eleitor").value = "adicionar"
	
	document.getElementById('form_usuarios_eleitor').onsubmit();

}