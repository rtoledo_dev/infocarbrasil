module AcessoriosHelper
  def imagem_principal(anuncio)
    url = anuncio.imagem_principal.arquivo.url rescue "foto_semfoto.jpg"
    image_tag url, :id => "quadro_imagens"
  end
  
  def imagens(anuncio)
    anuncio.anuncio_imagens.collect{|t|
      link_to_function(image_tag(t.arquivo.url(:resultado_busca)),"$('quadro_imagens').src='#{t.arquivo.url}'")
    }.join " "
  end
  
  
  def tipo_acessorios
    opcoes = [["Outro",nil]]
    opcoes += TipoAcessorio.all(:select => "id,descricao", :order => "descricao").collect{|t| [t.descricao,t.id]}
    opcoes
  end
end
