module LojasHelper
  def imagem_principal(loja)
    url = loja.imagem_principal.arquivo.url rescue "foto_semfoto.jpg"
    image_tag url, :id => "quadro_imagens"
  end
  
  def imagens(loja)
    loja.foto_lojas.collect{|t|
      link_to_function(image_tag(t.arquivo.url(:resultado_busca)),"$('quadro_imagens').src='#{t.arquivo.url}'")
    }.join " "
  end
end
