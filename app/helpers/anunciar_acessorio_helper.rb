module AnunciarAcessorioHelper
  
  def planos
    Plano.all(:conditions => ["tipo = ?",Plano::TIPO_ACESSORIO], :order => "valor")
  end
  
  
  def tipo_acessorios
    opcoes = [["Outro",nil]]
    opcoes += TipoAcessorio.all(:select => "id,descricao", :order => "descricao").collect{|t| [t.descricao,t.id]}
    opcoes
  end
end
