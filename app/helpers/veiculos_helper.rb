module VeiculosHelper
  def imagem_principal(anuncio)
    url = anuncio.imagem_principal.arquivo.url rescue "foto_semfoto.jpg"
    image_tag url, :id => "quadro_imagens"
  end
  
  def imagens(anuncio)
    anuncio.anuncio_imagens.collect{|t|
      link_to_function(image_tag(t.arquivo.url(:resultado_busca)),"$('quadro_imagens').src='#{t.arquivo.url}'")
    }.join " "
  end
  
  def tipo_veiculos
    opcoes = [["Selecione -->",nil]]
    opcoes += TipoVeiculo.all(:select => "id,descricao", :order => "descricao").collect{|t| [t.descricao,t.id]}
    opcoes
  end
  
  def marcas
    opcoes = [["Selecione -->",nil]]
    opcoes += Marca.all(:select => "id,descricao", :conditions => ["tipo_veiculo_id = ?",params[:busca_anuncio_veiculo][:tipo_veiculo_id].to_i], :order => "descricao").collect{|t| [t.descricao,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def modelos
    opcoes = [["Selecione -->",nil]]
    opcoes += Modelo.all(:select => "id,descricao", :conditions => ["marca_id = ?",params[:busca_anuncio_veiculo][:marca_id].to_i], :order => "descricao").collect{|t| [t.descricao,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def cores
    opcoes = [["Selecione -->",nil]]
    opcoes += Cor.all(:select => "id,descricao", :order => "descricao").collect{|t| [t.descricao,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def combustiveis
    opcoes = [["Selecione -->",nil]]
    opcoes += AnuncioVeiculo::COMBUSTIVEIS.collect{|i,valor| [valor,i]}
  end
  
  
  def opcionais
    tipo_veiculo = TipoVeiculo.find(params[:busca_anuncio_veiculo][:tipo_veiculo_id].to_i)
    tipo_veiculo.opcionais
  rescue
    []
  end
  
  def estados 
    opcoes = [["Selecione -->",nil]]
    opcoes += Estado.all(:select => "id,nome", :conditions => ["pais_id = ?",params[:busca_anuncio_veiculo][:pais_id].to_i], :order => "nome").collect{|t| [t.nome,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def cidades 
    opcoes = [["Selecione -->",nil]]
    opcoes += Cidade.all(:select => "id,nome", :conditions => ["estado_id = ?",params[:busca_anuncio_veiculo][:estado_id].to_i], :order => "nome").collect{|t| [t.nome,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
end
