module ApplicationHelper

  def flash_script(arquivo,largura,altura,transaparencia = nil)
    nome_flash = arquivo.split("/").last.split(".").first
    codigo_js = "var flash_item = new Flash(\"#{arquivo}\", \"#{nome_flash}\", \"#{largura}\", \"#{altura}\");"
    codigo_js << "flash_item.addParameter(\"showMenu\", \"false\");"
    codigo_js << "flash_item.addParameter(\"wmode\", \"#{transaparencia}\");" if transaparencia
    codigo_js << "flash_item.write();"
    
    javascript_tag codigo_js
  end


  def valor_publico(valor)
    valor.nil? || valor <= 0 ? "Consultar" : valor.real_formatado
  end
  
  def valor_plano(valor)
    valor.nil? || valor <= 0 ? "GRÁTIS" : valor.real_contabil
  end

  # Mensagens por javascript
  def flash_message_front
    saida = ''
    [:notice, :info, :warning, :error].each {|type|
      if flash[type]
        saida << javascript_tag("alert('#{flash[type]}')")
      end
    }
    saida
  end
  
  def menu
    itens = []
    itens << ["menu_paginic",{:controller => :home}]
    itens << ["menu_comprar",{:controller => :veiculos}]
    itens << ["menu_vender",{:controller => :anunciar_veiculo}]
    itens << ["menu_servicos",{:controller => :servicos}]
    itens << ["menu_cadastrese",{:controller => :cadastro, :tipo => Anunciante::ANUNCIANTE}]
    itens << ["menu_pedido",{:controller => :pedidos}]
    itens << ["menu_lojas",{:controller => :lojas}]
    itens << ["menu_noticias",{:controller => :noticias}]
    itens << ["menu_contato",{:controller => :contato}]
    links = itens.collect{|imagem,caminho| content_tag(:li,link_to(image_tag("#{imagem}.jpg"), caminho))}
    content_tag :ul, links.join(content_tag(:li,image_tag("menu_spacer.jpg")))
  end
 
  
  def submenu
    itens = []
    itens << [content_tag(:strong,"Anunciar Veículos"),{:controller => :anunciar_veiculo}]
    itens << [content_tag(:strong,"Anunciar Acessórios"),{:controller => :anunciar_acessorio}]
    itens << ["Minha conta",{:controller => :painel_anunciante}]
    itens << ["Acessórios",{:controller => :acessorios}]
    itens << ["Veículos",{:controller => :veiculos}]
    unless @anunciante_logado
      itens << ["Anunciante",{:controller => :cadastro, :tipo => Anunciante::ANUNCIANTE}]
      itens << ["Loja",{:controller => :cadastro, :tipo => Anunciante::LOJA}]
    else
      itens << ["Sair",{:controller => :login, :action => :deslogar}]
    end
    links = itens.collect{|texto,caminho| content_tag(:li,link_to(texto, caminho))}
    content_tag :ul, links.join(content_tag(:li,"  |  "))
  end
  
  def link_anuncios(texto, objeto)
    controller = :veiculos
    controller = :acessorios if objeto.tipo == Plano::TIPO_ACESSORIO
    link_to texto, :controller => controller, :action => :ver, :id => objeto
  end
  
  def titulo_anuncio(anuncio)
    link_anuncios(anuncio.extensao.nome,anuncio)
  end
  
  def link_noticia(texto, noticia)
    link_to texto, :controller => :noticias, :action => :ver,  :id => noticia
  end
  
  def link_loja(texto,loja)
    link_to texto, :controller => :lojas, :action => :estoque, :id => loja
  end
  
  
  def imagem_resultado_busca(objeto)
    url = objeto.imagem_principal.arquivo.url(:resultado_busca) rescue "foto_semfoto.jpg"
    image_tag url
  end
  
  def imagem_destaque(objeto)
    url = objeto.imagem_principal.arquivo.url(:destaque_medio) rescue "foto_semfoto.jpg"
    image_tag url
  end
  
  
  def imagem_principal_noticia(noticia)
    image_tag noticia.foto_principal.arquivo.url(:thumb) rescue ""
  end
  
  
  
  def busca_tipo_veiculos
    opcoes = [["Indiferente",nil]]
    opcoes += TipoVeiculo.all(:order => "descricao").collect{|t| [t.descricao,t.id]}
  end
  
  
  def busca_tipo_acessorios
    opcoes = [["Indiferente",nil]]
    opcoes += TipoAcessorio.all(:order => "descricao").collect{|t| [t.descricao,t.id]}
  end
  
  
  def busca_marcas
    opcoes = [["Indiferente",nil]]
    opcoes += Marca.all(:conditions => {:tipo_veiculo_id => @busca_anuncio_veiculo.tipo_veiculo_id}, :order => "descricao").collect{|t| [t.descricao,t.id]}
  rescue
    opcoes = [["Indiferente",nil]]
  end
  
  
  def busca_modelos
    opcoes = [["Indiferente",nil]]
    opcoes += Modelo.all(:conditions => {:marca_id => @busca_anuncio_veiculo.marca_id}, :order => "descricao").collect{|t| [t.descricao,t.id]}
  rescue
    opcoes = [["Indiferente",nil]]
  end
  
  
  def busca_faixas_precos_veiculos
    opcoes = []
    AnuncioVeiculo::FAIXAS_PRECOS.each do |faixa|
      if faixa >= 0
        opcoes << [faixa.real_contabil,faixa.to_s]
      else
        opcao_acima = AnuncioVeiculo::FAIXAS_PRECOS[AnuncioVeiculo::FAIXAS_PRECOS.size - 2]
        opcoes << ["Acima de #{opcao_acima.real_contabil}",(opcao_acima + 1).to_s]
      end
    end
    opcoes
  end
  
  def busca_faixas_precos_veiculos_final
    opcoes = []
    opcoes = busca_faixas_precos_veiculos
    opcoes[opcoes.length-1] = nil
    opcoes.compact!
    opcoes
  end
  
  
  def busca_faixas_precos_acessorios
    opcoes = []
    AnuncioAcessorio::FAIXAS_PRECOS.each do |faixa|
      if faixa >= 0
        opcoes << [faixa.real_contabil,faixa.to_s]
      else
        opcao_acima = AnuncioAcessorio::FAIXAS_PRECOS[AnuncioAcessorio::FAIXAS_PRECOS.size - 2]
        opcoes << ["Acima de #{opcao_acima.real_contabil}",(opcao_acima + 1).to_s]
      end
    end
    opcoes
  end
  
  def busca_faixas_precos_acessorios_final
    opcoes = []
    opcoes = busca_faixas_precos_acessorios
    opcoes[opcoes.length-1] = nil
    opcoes.compact!
    opcoes
  end  
  
  def busca_lojas
    opcoes = [["Selecione -->",nil]]
    opcoes += Loja.all(:include => :anunciante).collect{|t| [t.anunciante.nome,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end

  def paises 
    opcoes = [["Selecione -->",nil]]
    opcoes += Pais.all(:select => "id,nome", :order => "nome").collect{|t| [t.nome,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end

  def mostrar_arquivo(banner)
    if banner.tipo == AnuncioBanner::TIPO_IMAGEM
      link_to_if !banner.link.blank?, image_tag(banner.arquivo.url, :size => "#{banner.width}x#{banner.height}"), banner.link, :target => "_blank"
    else  
      flash_script banner.arquivo.url, banner.width, banner.height, banner.wmode
    end
  rescue
    nil
  end
  
  
  def banners_laterais
    @banners_laterais ||= AnuncioBanner.all :conditions => (["anuncios.status = ? and planos.destaque = ?", true, false]),
                      :include => {:anuncio => :plano},
                      :order => "rand()",
                      :limit => 12
  end
  
  def banner_topo
    mostrar_arquivo(AnuncioBanner.first(:include => {:anuncio => :plano},
                                        :conditions => ["anuncios.status=? and planos.destaque=?",true,true],
                                        :order => "rand()")) || banner_topo_padrao
  end
  
  
  def banner_topo_padrao
    banner = Banner.first :conditions => {:localizacao => Banner::LOCALIZACAO_TOPO}, :order => "ativo DESC, created_at DESC"
    
    if banner.tipo == Banner::TIPO_FLASH
      flash_script banner.arquivo.url, 468, 60, false
    else
      link_to image_tag(banner.arquivo.url(:topo)),banner.link,{:target => "_blank"}
    end
  rescue
    nil
  end
  
  
  def banner_lateral_padrao
    banner = Banner.first :conditions => {:localizacao => Banner::LOCALIZACAO_LATERAL}, :order => "ativo DESC, created_at DESC"
    
    if banner.tipo == Banner::TIPO_FLASH
      flash_script banner.arquivo.url, 120, 60, false
    else
      link_to image_tag(banner.arquivo.url(:lateral)),banner.link,:target => "_blank"
    end
  rescue
    nil
  end
end
