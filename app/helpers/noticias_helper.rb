module NoticiasHelper


  def imagem_noticia_destaque(noticia)
    content_tag :td, (image_tag noticia.foto_principal.arquivo.url(:destaque))
  rescue
    ""
  end
  
  def descricao_topico_noticia(topico_noticia)
    ["(#{topico_noticia.noticias_ativas.count})", topico_noticia.descricao].join " "
  end
  
  
  def topico_noticias
    @topico_noticias ||= TopicoNoticia.all(:order => "descricao")
  end
  
  
  def imagem_principal(noticia)
    url = noticia.foto_principal.arquivo.url
    image_tag url, :id => "quadro_imagens"
  rescue
    ""
  end
  
  def imagens(noticia)
    noticia.foto_noticias.collect{|t|
      link_to_function(image_tag(t.arquivo.url(:thumb)),"$('quadro_imagens').src='#{t.arquivo.url}'")
    }.join " "
  end
end
