module Admin::BannersHelper
  
  
  def anunciantes_busca
    opcoes = [["Indiferente",nil]]
    opcoes += Anunciante.all(:conditions => ["tipo = ?",Anunciante::LOJA],:include => :loja, :order => "nome").collect{|t| ["#{t.nome} (#{t.loja.cnpj})",t.id]}
  rescue
    opcoes = [["Indiferente",nil]]
  end
  
  
  def status
    opcoes = [["Indiferente",nil]]
    opcoes += Anuncio::STATUS.collect{|valor,descricao| [descricao,valor]}
  rescue
    opcoes = [["Indiferente",nil]]
  end
  
  
  def planos
    opcoes = [["Selecione -->",nil]]
    opcoes += Plano.all(:conditions => ["tipo = ?",Plano::TIPO_BANNER], :order => "valor").collect{|t| ["#{t.descricao} (#{t.valor.real_contabil})",t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  
  def anunciantes
    opcoes = [["Selecione -->",nil]]
    opcoes += Anunciante.all(:conditions => ["tipo = ?",Anunciante::LOJA],:include => :loja, :order => "nome").collect{|t| ["#{t.nome} (#{t.loja.cnpj})",t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  
  def tipos
    opcoes = [["Selecione -->",nil]]
    opcoes += AnuncioBanner::TIPOS.collect{|opcao,valor| [valor,opcao]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def mostrar_arquivo(banner)
    if banner.tipo == AnuncioBanner::TIPO_IMAGEM
      image_tag banner.arquivo.url
    else  
      flash_script banner.arquivo.url, 120, 60, false
    end
  end
end
