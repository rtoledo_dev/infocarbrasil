module Admin::FotoNoticiasHelper
  def noticias
    opcoes = [["Selecione -->",nil]]
    opcoes += Noticia.all.collect{|t| [t.titulo,t.id]}
    opcoes
  end
  
  
  def nova_foto?
    @foto_noticia.nil? || @foto_noticia.new_record?
  end
end
