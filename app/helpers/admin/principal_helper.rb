module Admin::PrincipalHelper
  
  def existem_anuncios_mais_vistos?
    @anuncios_mais_vistos.size > 0
  rescue
    false
  end
  
  
  def existem_anunciantes?
    @anunciantes.size > 0
  rescue
    false
  end
  
  
  def existem_anuncios_expirados?
    @anuncios_expirados.size > 0
  rescue
    false
  end
  
  
  def existem_anuncios_a_expirar?
    @anuncios_a_expirar.size > 0
  rescue
    false
  end
  
  
  
  def titulo_anuncio(anuncio)
    "<strong>#{link_anuncios(anuncio.extensao.nome,anuncio)}</strong>.</strong>,"
  end
  
  
  def link_anuncios(texto, objeto)
    link_to texto, :controller => :anuncios, :action => :visualizar, :id => objeto
  end
  
  
  def link_anunciantes(texto, objeto)
    link_to texto, :controller => :anunciantes, :action => :editar, :id => objeto
  end
end
