module Admin::AnunciosHelper
  def acoes(anuncio)
    if anuncio.status
      link_to_remote image_tag("icons/delete.gif", :title => "Negar"), :url => {:action => :expirar, :id => anuncio}
    elsif anuncio.pode_reativar? || !anuncio.pagamento_efetuado?
      link_to_remote image_tag("icons/ok.gif", :title => "Autorizar"), :url => {:action => :reativar, :id => anuncio}
    end
  end
  
  def status_anuncio(anuncio)
    if anuncio.status
      "Ativo"
    elsif !anuncio.status && anuncio.expira_em >= Date.today
      "Inativo"
    else
      "Expirado"
    end
  end
 
  def situacao_financeira(anuncio)
    anuncio.pagamento_efetuado? ? "Pago" : "Pendente de Pagamento"
  end
  
  def tipos_planos
    opcoes = [["Selecione -->",nil]]
    opcoes += Plano::TIPOS.collect{|i,valor| [valor,i]}
    opcoes
  end
  
  def get_tipo(tipo)
    Plano::TIPOS[tipo]
  end  

  def get_combustivel(tipo)
    AnuncioVeiculo::COMBUSTIVEIS[tipo]
  end  
  
  
  def render_partial_tipo_anuncio(tipo)
    case tipo
      when Plano::TIPO_VEICULO
        render :partial => "form_veiculo"
      when Plano::TIPO_BANNER
        render :partial => "form_banner"
      when Plano::TIPO_ACESSORIO
        render :partial => "form_acessorio"      
    end
  end
  
  def situacao(anuncio)
    if anuncio.status == false || anuncio.expirado?
      image_tag "icons/delete.gif", :title => "Expirado"
    elsif anuncio.pagamento_efetuado?
      image_tag "icons/tick.gif", :title => "OK"
    else
      image_tag "icons/clock.gif", :title => "Aguardando"      
    end
  end
  
  def imagem_principal(anuncio)
    url = anuncio.imagem_principal.arquivo.url rescue "foto_semfoto.jpg"
    image_tag url, :id => "quadro_imagens"
  end
  
  def imagens(anuncio)
    anuncio.anuncio_imagens.collect{|t|
      link_to_function(image_tag(t.arquivo.url(:resultado_busca)),"$('quadro_imagens').src='#{t.arquivo.url}'")
    }.join " "
  end
  
end
