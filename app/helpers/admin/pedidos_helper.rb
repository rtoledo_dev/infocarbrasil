module Admin::PedidosHelper
  
  def opcoes_analise_busca
    opcoes = []
    opcoes << ["Indiferente",nil]
    opcoes << ["Pendente",false]
    opcoes << ["Analisado",true]
    
    opcoes
  end
  
  def opcoes_analise
    opcoes = []
    opcoes << ["Selecione -->",nil]
    opcoes << ["Pendente",false]
    opcoes << ["Analisado",true]
    
    opcoes
  end
  
  
  def descricao_interessado(pedido)
    [pedido.interessado,pedido.celular,pedido.telefone,pedido.email].compact.join ", "
  end
end
