module Admin::BannersPadroesHelper
  
  def tipos_busca
    opcoes = [["Indiferente",nil]]
    opcoes += Banner::TIPOS.collect{|id,descricao| [descricao,id]}
    opcoes
  end
  
  def localizacoes_busca
    opcoes = [["Indiferente",nil]]
    opcoes += Banner::LOCALIZACOES.collect{|id,descricao| [descricao,id]}
    opcoes
  end
  
  
  def ativo_busca
    opcoes = [["Indiferente",nil]]
    opcoes << ["Sim",true]
    opcoes << ["Não",false]
    opcoes
  end
  
  def tipos
    opcoes = [["Selecione -->",nil]]
    opcoes += Banner::TIPOS.collect{|id,descricao| [descricao,id]}
    opcoes
  end
  
  
  def localizacoes
    opcoes = [["Selecione -->",nil]]
    opcoes += Banner::LOCALIZACOES.collect{|id,descricao| [descricao,id]}
    opcoes
  end
  
  
  def ativo
    opcoes = [["Selecione -->",nil]]
    opcoes << ["Sim",true]
    opcoes << ["Não",false]
    opcoes
  end
end
