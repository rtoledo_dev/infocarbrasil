module Admin::MarcasHelper
  def tipos_veiculos
    opcoes = [["Selecione -->",nil]]
    opcoes += TipoVeiculo.all.collect{|t| [t.descricao,t.id]}
    opcoes
  end
end
