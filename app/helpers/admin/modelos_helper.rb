module Admin::ModelosHelper
  def marcas
    opcoes = [["Selecione -->",nil]]
    opcoes += Marca.all.collect{|t| [t.descricao,t.id]}
    opcoes
  end
end
