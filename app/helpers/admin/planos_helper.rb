module Admin::PlanosHelper
  def tipos_planos
    opcoes = [["Selecione -->",nil]]
    opcoes += Plano::TIPOS.collect{|i,valor| [valor,i]}
    opcoes
  end
  
  def mostrar_destaque(plano)
    if !plano.new_record?
      return true 
    end  
    return false
  end
  
  def get_tipo(tipo)
    Plano::TIPOS[tipo]
  end
end
