module PedidosHelper
  def tipo_veiculos
    opcoes = [["Selecione -->",nil]]
    opcoes += TipoVeiculo.all(:select => "id,descricao", :order => "descricao").collect{|t| [t.descricao,t.id]}
    opcoes
  end
  def marcas
    opcoes = [["Selecione -->",nil]]
    opcoes += Marca.all(:select => "id,descricao", :conditions => ["tipo_veiculo_id = ?",params[:pedido][:tipo_veiculo_id].to_i], :order => "descricao").collect{|t| [t.descricao,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def modelos
    opcoes = [["Selecione -->",nil]]
    opcoes += Modelo.all(:select => "id,descricao", :conditions => ["marca_id = ?",params[:pedido][:marca_id].to_i], :order => "descricao").collect{|t| [t.descricao,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def cores
    opcoes = [["Selecione -->",nil]]
    opcoes += Cor.all(:select => "id,descricao", :order => "descricao").collect{|t| [t.descricao,t.id]}
  rescue
    opcoes = [["Selecione -->",nil]]
  end
  
  def combustiveis
    opcoes = [["Selecione -->",nil]]
    opcoes += AnuncioVeiculo::COMBUSTIVEIS.collect{|i,valor| [valor,i]}
  end
  
end
