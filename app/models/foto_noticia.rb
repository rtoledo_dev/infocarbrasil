class FotoNoticia < ActiveRecord::Base
  belongs_to :noticia
  
  validates_presence_of :descricao, :noticia_id
  
  has_attached_file :arquivo, 
    :styles => { :destaque => "201x151#", :thumb => "81x61#", :list => "50x50#", :big => "900>", :normal => "312x234" }, 
    :default_style => :normal
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:busca]}%"
    
    self.paginate :conditions => ["titulo like ? OR autor like ? OR fonte like ? OR entrada like ? OR texto like ? OR descricao LIKE ? OR arquivo_file_name like ?",
                                  busca,busca,busca,busca,busca,busca,busca],
                  :joins => "INNER JOIN noticias ON foto_noticias.noticia_id = noticias.id",
                  :order => "noticias.created_at,arquivo_file_name", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
end
