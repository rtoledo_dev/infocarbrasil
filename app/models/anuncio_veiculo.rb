class AnuncioVeiculo < ActiveRecord::Base
  COMBUSTIVEL_GASOLINA = 0
  COMBUSTIVEL_ALCOOL = 1
  COMBUSTIVEL_DIESEL = 2
  COMBUSTIVEL_FLEX = 3
  
  COMBUSTIVEIS = {COMBUSTIVEL_GASOLINA => "Gasolina",
                  COMBUSTIVEL_ALCOOL => "Álcool",
                  COMBUSTIVEL_DIESEL => "Diesel",
                  COMBUSTIVEL_FLEX => "Flex"}
                  
  FAIXAS_PRECOS = [0,5000,10000,15000,20000,25000,30000,35000,40000,
                    45000,50000,55000,60000,65000,70000,75000,80000,
                    85000,90000,95000, 100000, -1]
  
  
  
  belongs_to :anuncio
  belongs_to :marca
  belongs_to :modelo
  belongs_to :tipo_veiculo
  belongs_to :pais
  belongs_to :estado
  belongs_to :cidade
  belongs_to :cor

  has_and_belongs_to_many :opcionais, :join_table => "opcionais_anuncio_veiculos"
  
  validates_presence_of :tipo_veiculo_id,:marca_id,:modelo_id, :cor_id, :pais_id, :estado_id, :cidade_id, :ano, :combustivel
  
  usar_como_dinheiro :valor
  attr_accessor :faixa_preco_inicial
  attr_accessor :faixa_preco_final
  
  def nome
    [self.marca.descricao,self.modelo.descricao,self.valor.real_formatado].join " | "
  end
  
  
  def nome_frontend
    ["#{self.marca.descricao} - #{self.modelo.descricao}",self.ano].join "<br />"
  end
  
  
  
  def buscar(pagina)
    condicoes = []
    parametros = []
    
    condicoes << "status = ?"
    parametros << true
    
    
    if self.tipo_veiculo_id
      condicoes << "tipo_veiculo_id = ?"
      parametros << self.tipo_veiculo_id
    end
    
    if self.marca_id
      condicoes << "marca_id = ?"
      parametros << self.marca_id
    end
    
    if self.modelo_id
      condicoes << "marca_id = ?"
      parametros << self.modelo_id
    end
    
    if self.cor_id
      condicoes << "cor_id = ?"
      parametros << self.cor_id
    end
    
    if self.pais_id
      condicoes << "pais_id = ?"
      parametros << self.pais_id
    end
    
    if self.estado_id
      condicoes << "estado_id = ?"
      parametros << self.estado_id
    end
    
    if self.cidade_id
      condicoes << "cidade_id = ?"
      parametros << self.cidade_id
    end
    
    if self.ano
      condicoes << "ano = ?"
      parametros << self.ano
    end
    
    if self.combustivel
      condicoes << "combustivel = ?"
      parametros << self.combustivel
    end
    
    if self.kilometragem
      condicoes << "kilometragem = ?"
      parametros << self.kilometragem
    end
    
    if self.faixa_preco_inicial.to_f > 0.0
      condicoes << "valor >=  ?"
      parametros << self.faixa_preco_inicial.to_f
    end
    
    if self.faixa_preco_final.to_f > 0.0
      condicoes << "valor <= ?"
      parametros << self.faixa_preco_final.to_f
    end
    
    if self.opcional_ids.size > 0
      condicoes << "anuncio_veiculos.id IN (SELECT anuncio_veiculo_id FROM opcionais_anuncio_veiculos WHERE opcional_id IN (?) )"
      parametros << self.opcional_ids
    end
    
    Anuncio.paginate :conditions => ([condicoes.join(" AND ")] + parametros),
                  :select => "anuncios.*",
                  :joins => "INNER JOIN anuncio_veiculos ON anuncio_veiculos.anuncio_id = anuncios.id",
                  :per_page => MAXIMO_POR_PAGINA, :page => pagina,
                  :order => "anuncio_veiculos.created_at DESC"
  end
end
