class Notificador < ActionMailer::Base

  def enviar_email_contato(contato)
    @from            = "contato@infocarbrasil.com.br"
    @recipients      = "contato@infocarbrasil.com.br"
    @reply_to        = contato.email
    @bcc             = "rodrigo@rtoledo.inf.br"
    @subject         = "Infocar - Contato enviado pelo site"
    @body["contato"] = contato
  end
  
  def enviar_email_proposta(envio_proposta)
    @recipients = envio_proposta.anuncio.anunciante.email
    @bcc        = [envio_proposta.anuncio.anunciante.email_secundario, "rodrigo@rtoledo.inf.br"].compact
    @from       = envio_proposta.email
    @subject    = "Infocar - Proposta de Negócios"
    @body["envio_proposta"] = envio_proposta
  end  

end
