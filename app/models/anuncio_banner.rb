class AnuncioBanner < ActiveRecord::Base
  TIPO_FLASH = 0
  TIPO_IMAGEM = 1
  TIPOS = {TIPO_FLASH => "Flash", TIPO_IMAGEM => "Imagem"}
  
  belongs_to :anuncio
  
  validates_presence_of :tipo
  
  has_attached_file :arquivo
  
  def nome
    arquivo_file_name
  end
  
  
  def self.pesquisa_admin(parametros_busca,pagina)
    condicoes = []
    parametros = []
    
    
    if parametros_busca.has_key?(:anunciante_id) && !parametros_busca[:anunciante_id].blank?
      condicoes << "anunciante_id = ?"
      parametros << parametros_busca[:anunciante_id]
    end
    
    if parametros_busca.has_key?(:status) && !parametros_busca[:status].blank?
      condicoes << "status = ?"
      parametros << parametros_busca[:status]
    end
    
    
    Anuncio.paginate :conditions => ([condicoes.join(" AND ")]+parametros),
                  :joins => "INNER JOIN anuncio_banners AS ab ON anuncios.id = ab.anuncio_id",
                  :order => "anuncios.expira_em DESC", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
end
