class AnuncioImagem < ActiveRecord::Base
  
  validates_presence_of :anuncio_id
  
  belongs_to :anuncio
  
  has_attached_file :arquivo,
    :styles => {:normal => "312x234>", :resultado_busca => "78x58#", :destaque_medio => "128x96#" },
    :default_style => :normal
end