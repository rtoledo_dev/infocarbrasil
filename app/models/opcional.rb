class Opcional < ActiveRecord::Base
  has_and_belongs_to_many :tipo_veiculos, :join_table => "opcionais_tipo_veiculos"
  has_and_belongs_to_many :anuncio_veiculos, :join_table => "opcionais_anuncio_veiculos"
  
  validates_presence_of :descricao
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:descricao]}%"
    self.paginate :conditions => ["opcionais.descricao like ?",busca],
                  :include => :tipo_veiculos,
                  :order => "opcionais.descricao", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
  
end
