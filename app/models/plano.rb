class Plano < ActiveRecord::Base
  TIPO_VEICULO = "AnuncioVeiculo"
  TIPO_BANNER = "AnuncioBanner"
  TIPO_ACESSORIO = "AnuncioAcessorio"
  
  TIPOS = {TIPO_VEICULO => "Veiculo",
           TIPO_BANNER => "Banner",
           TIPO_ACESSORIO => "Acessório"}
  
  has_many :anuncios, :dependent => :destroy
  
  validates_presence_of :descricao,:vigencia, :valor, :tipo
  validates_uniqueness_of :descricao, :scope => [:descricao,:tipo]
  
  usar_como_dinheiro :valor  
  
  def expira_em(data = nil)
    data ||= Date.today
    data + self.vigencia
  end
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:descricao]}%"
    self.paginate :conditions => ["planos.descricao like ? or planos.tipo like ?",busca, busca],
                  :order => "planos.descricao, planos.tipo", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end

end
