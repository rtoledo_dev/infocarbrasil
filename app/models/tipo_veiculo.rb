class TipoVeiculo < ActiveRecord::Base

  has_and_belongs_to_many :opcionais
  
  validates_presence_of :descricao
  validates_uniqueness_of :descricao
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:descricao]}%"
    self.paginate :conditions => ["descricao like ?",busca], :order => "descricao", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
end
