class AnuncioAcessorio < ActiveRecord::Base

  FAIXAS_PRECOS = [0,50,100,150,200,300,400,-1]
  
  belongs_to :anuncio
  belongs_to :tipo_acessorio
  
  attr_accessor :faixa_preco_inicial
  attr_accessor :faixa_preco_final
  
  validate :validar_tipo_outro
  
  def validar_tipo_outro
    if self.tipo_acessorio_id.blank? && self.tipo_acessorio_outro.blank?
      self.errors.add :tipo_acessorio_id, "não pode estar em branco"
    end
  end
  
  usar_como_dinheiro :valor

  
  
  def nome
    tipo = self.tipo_acessorio.descricao rescue self.tipo_acessorio_outro
    [tipo,self.valor.real_formatado].join " | "
  end
  
  
  
  # Busca utilizada para filtrar dados a partir da página de busca avançada
  def buscar(pagina)
    condicoes = []
    parametros = []
    
    condicoes << "status = ?"
    parametros << true
    
    unless self.tipo_acessorio_id.nil?
      condicoes << "tipo_acessorio_id = ?"
      parametros << self.tipo_acessorio_id
    end
    
    if self.faixa_preco_inicial.to_f > 0.0
      condicoes << "valor >= ?"
      parametros << self.faixa_preco_inicial.to_f
    end  
    if self.faixa_preco_final.to_f > 0.0
      condicoes << "valor <= ?"
      parametros << self.faixa_preco_final.to_f
    end    
    
    
    Anuncio.paginate :conditions => ([condicoes.join(" AND ")] + parametros),
                  :select => "anuncios.*",
                  :joins => "INNER JOIN anuncio_acessorios ON anuncio_acessorios.anuncio_id = anuncios.id",
                  :per_page => MAXIMO_POR_PAGINA, :page => pagina,
                  :order => "anuncio_acessorios.created_at DESC"
  end
end
