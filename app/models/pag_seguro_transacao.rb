require 'net/http'
require 'uri'

class PagSeguroTransacao < ActiveRecord::Base
  URL_PAGSEGURO = 'https://pagseguro.uol.com.br/Security/NPI/Default.aspx'
  VENDEDOR_EMAIL = 'rodrigo.toledo@agence.com.br'
  FORMA_PAGAMENTO = 'CP'
  MOEDA = 'BRL'
  PARAMETROS  = {:Comando => 'validar', :Token => '136BE6697307BCE0BBE9A209D03B6B3F'}
  
  set_table_name "PagSeguroTransacoes"
  set_primary_key "TransacaoID"
  
  has_one     :produto, :class_name => "PagSeguroTransacaoProduto", :foreign_key => "TransacaoID"
  has_many    :produtos, :class_name => "PagSeguroTransacaoProduto", :foreign_key => "TransacaoID"
  belongs_to  :anuncio, :foreign_key => "Referencia"
  
  validates_presence_of :TransacaoID,
                        :NumItens,
                        :VendedorEmail,
                        :Referencia,
                        :TipoPagamento,
                        :StatusTransacao,
                        :CliNome,
                        :CliEmail
                        
  before_validation :validar_email_vendedor,
                    :validar_valor
  
  attr_accessor :erro
  
  # Salva por completo o retorno de uma transacao
  def self.salvar_retorno(parametros)
    
    # Responde ao retorno com token valido
    if self.responder_retorno(parametros)

      # somente se a transacao nao existir o passo eh avancado
      if !PagSeguroTransacao.exists?(parametros[:TransacaoID])
        
        # Inicia uma transacao
        transacao = PagSeguroTransacao.new
        # Parametros setados para transacao e para seus produtos
        transacao.set_parametros(parametros)
  
        # Teoricamente salvando as transacoes, os produtos da mesma seriam salvos
        unless transacao.save
          raise "Não foi possível salvar a transação. Erros: #{transacao.errors}"
        end
        true
      else
        raise "Já existe uma transação com este identificador (#{parametros[:TransacaoID]})"
      end
    else
      raise "Falha ao entrar em contato com PagSeguro por meio de post"
    end
    
    
  rescue => mensagem
    self.erro = mensagem.message
    false
  end
  
  
  # Post de confirmacao do carrinho
  # <tt>parametros</tt>: Array com parametros enviado por pagseguro
  def self.responder_retorno(parametros)
    resultado_post = Net::HTTP.post_form(URI.parse(URL_PAGSEGURO),(parametros.merge PagSeguroTransacao::PARAMETROS))
    resultado_post.include? "VERIFICADO"
  end
  
  
  def set_parametros(parametros)
    PagSeguroTransacao.columns.each do |coluna|
      coluna = column.to_s.to_symb
      write_attribute(coluna,parametros[coluna]) if parametros.has_key?(coluna) && !parametros[coluna].nil?
    end
    
    self.add_produtos(parametros)
  end
  
  
  def add_produtos(parametros)
    1.upto(parametros[:NumItens].to_i) do |posicao|
      produto = PagSeguroTransacaoProduto.new
      produto.ProdID          = parametros[PagSeguroTransacaoProduto("ProdID",posicao)]
      produto.ProdDescricao   = parametros[PagSeguroTransacaoProduto("ProdDescricao",posicao)]
      produto.ProdValor       = parametros[PagSeguroTransacaoProduto("ProdValor",posicao)]
      produto.ProdQuantidade  = parametros[PagSeguroTransacaoProduto("ProdQuantidade",posicao)]
      produto.ProdFrete       = parametros[PagSeguroTransacaoProduto("ProdFrete",posicao)]
      produto.ProdExtras      = parametros[PagSeguroTransacaoProduto("ProdExtras",posicao)]
      self.produtos << produto
    end
  end
  
  
  def validar_email_vendedor
    if self.VendedorEmail != VENDEDOR_EMAIL
      self.errors.add :VendedorEmail, "Email de vendedor incorreto"
    end
  end
  
  def validar_valor
    if self.produto.ProdValor != self.anuncio.valor_a_pagar
      self.errors.add :Referencia, "Valor incorreto. Experado: #{self.anuncio.valor_a_pagar.real_contabil}, valor atual: #{self.produto.ProdValor.real_contabil}"
    end
  end
    
end
