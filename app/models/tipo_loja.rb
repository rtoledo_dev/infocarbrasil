class TipoLoja < ActiveRecord::Base
  has_and_belongs_to_many :lojas, :join_table => "lojas_tipo_lojas"
  
  def lojas_paginadas(pagina)
    self.lojas.paginate :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
end
