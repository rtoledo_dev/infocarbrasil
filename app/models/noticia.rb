class Noticia < ActiveRecord::Base
  belongs_to :topico_noticia
  has_many :foto_noticias
  has_one :foto_principal, :class_name => "FotoNoticia", :order => "principal DESC"
  
  
  validates_presence_of :titulo,:autor,:entrada,:texto,:created_at,:topico_noticia_id
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:titulo]}%"
    self.paginate :conditions => ["titulo like ? OR autor like ? OR fonte like ? OR entrada like ? OR texto like ? OR topico_noticias.descricao LIKE ?",busca,busca,busca,busca,busca,busca],
                  :include => :topico_noticia,
                  :order => "topico_noticias.descricao,titulo", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
  
end