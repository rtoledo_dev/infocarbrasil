class Pedido < ActiveRecord::Base
  validates_presence_of :interessado, :email

  belongs_to :modelo
  belongs_to :marca
  belongs_to :tipo_veiculo
  belongs_to :cor
  
  usar_como_dinheiro :valor
  
  def self.buscar_por_anuncios(anuncios)
    ids = []
    anuncios.each do |anuncio|
      condicoes = []
      parametros = []
      
      condicoes << "tipo_veiculo_id = ?"
      parametros << anuncio.anuncio_veiculo.tipo_veiculo_id
      
      condicoes << "modelo_id = ?"
      parametros << anuncio.anuncio_veiculo.modelo_id
      
      condicoes << "marca_id = ?"
      parametros << anuncio.anuncio_veiculo.marca_id
      
      condicoes << "cor_id = ?"
      parametros << anuncio.anuncio_veiculo.cor_id
      
      condicoes << "ano = ?"
      parametros << anuncio.anuncio_veiculo.ano
      
      condicoes << "valor = ?"
      parametros << anuncio.anuncio_veiculo.valor
      
      condicoes << "combustivel = ?"
      parametros << anuncio.anuncio_veiculo.combustivel
      
      condicoes << "kilometragem = ?"
      parametros << anuncio.anuncio_veiculo.kilometragem
      
      
      ids += Pedido.all(:select => "id", :conditions => ([condicoes.join(" OR ")]+parametros), :order => "created_at,interessado")
    end
    
    Pedido.all(:conditions => ["id in(?)",ids.uniq]) rescue []
  end
  
  def self.pesquisa_admin(parametros,pagina)
    
    condicoes = {:analisado => parametros[:analisado]} unless parametros[:analisado].blank?
    
    self.paginate :conditions => condicoes,
                  :order => "created_at DESC", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
end
