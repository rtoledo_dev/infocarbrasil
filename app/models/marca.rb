class Marca < ActiveRecord::Base
  belongs_to :tipo_veiculo
  
  validates_presence_of :descricao, :tipo_veiculo_id
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:descricao]}%"
    self.paginate :conditions => ["marcas.descricao like ? OR tipo_veiculos.descricao like ?",busca,busca],
                  :include => :tipo_veiculo,
                  :order => "tipo_veiculos.descricao,marcas.descricao", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
end
