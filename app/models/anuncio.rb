class Anuncio < ActiveRecord::Base

  A_EXPIRAR = 0
  EXPIRADO = 1

  STATUS = {A_EXPIRAR => "A Expirar",EXPIRADO => "Expirado"}


  belongs_to :plano
  belongs_to :anunciante
  has_many :anuncio_imagens, :class_name => "AnuncioImagem", :dependent => :destroy
  has_one :imagem_principal, :class_name => "AnuncioImagem"
  has_one :anuncio_veiculo, :dependent => :destroy, :foreign_key => "anuncio_id"
  has_one :anuncio_acessorio, :dependent => :destroy, :foreign_key => "anuncio_id"
  has_one :anuncio_banner, :dependent => :destroy, :foreign_key => "anuncio_id"
  has_one :transacao, :foreign_key => "Referencia", :class_name => "PagSeguroTransacao"

  usar_como_dinheiro :valor_a_pagar

  attr_protected :tipo
  attr_accessor :validar_anuncio

  validates_presence_of :descricao,:plano_id
  validates_presence_of :anunciante_id, :if => Proc.new{|t| t.validar_anuncio}

  after_create :set_expiracao, :set_tipo, :set_pag_seguro, :set_status


  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:descricao]}%"
    self.paginate :conditions => ["anuncios.descricao like ?", busca],
    :include => :anunciante,
    :order => "anuncios.descricao",
    :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end

  def self.apagar_incompletos
    Anuncio.destroy_all(["anunciante_id IS NULL AND created_at < ?",Time.now - 1.hour])
    Anuncio.all.collect{|t| t.destroy if t.extensao.nil? }
  end

  def self.expirar_anuncios
    Anuncio.all(:conditions => ["expira_em < ?",Date.today]).collect{|t| t.update_attribute(:status,false)}
  end

  def self.anunciante_anuncios(parametros, pagina)
    conditions = " anunciante_id = " + parametros[:anunciante].to_s

    if parametros[:tipo] && !parametros[:tipo].empty?
      conditions << " and anuncios.tipo = '" +  parametros[:tipo] + "'"
    end

    if parametros[:status] && !parametros[:status].empty? && parametros[:status].to_i == EXPIRADO
      conditions << " and anuncios.status = 0"
    end

    self.paginate :conditions => conditions,
    :order => "anuncios.expira_em DESC, anuncios.status",
    :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end


  def self.expirar(id)
    anuncio = Anuncio.find(id)
    anuncio.status = false
    anuncio.save
  rescue
    false
  end


  def self.reativar(id)
    anuncio = Anuncio.find(id)
    if anuncio.pode_reativar?
      anuncio.update_attribute(:status, true)
      true
    else
      anuncio.update_attribute(:status, false)
      false
    end
  end

  def self.efetuar_pagamento(id)
    anuncio = Anuncio.find(id)
    anuncio.update_attribute(:status, true)
    anuncio.update_attribute(:data_pagamento, Date.today)
    anuncio.update_attribute(:valor_a_pagar, nil)
    true
  end


  def gerar_boleto
    boleto = BancoBrasil.new
    boleto.cedente = BOLETO_CEDENTE
    boleto.documento_cedente = BOLETO_DOCUMENTO_CEDENTE
    boleto.sacado = self.anunciante.nome
    boleto.documento_sacado = self.anunciante.cpf.to_s.tr('.','').tr('-','')
    boleto.valor = self.valor_a_pagar.to_f
    boleto.valor_documento = (boleto.quantidade * boleto.valor)
    boleto.aceite = "S"
    boleto.agencia = BOLETO_AGENCIA
    boleto.agencia_dv = boleto.modulo11_9to2_bb(boleto.agencia)
    boleto.conta_corrente = BOLETO_CONTA_CORRENTE
    boleto.conta_corrente_dv = boleto.modulo11_9to2_bb(boleto.conta_corrente)
    boleto.convenio = BOLETO_CONVENIO
    boleto.nosso_numero = self.id.to_s
    boleto.nosso_numero_dv = boleto.modulo11_9to2_bb("#{boleto.convenio}#{boleto.nosso_numero}")
    boleto.numero_documento = ["",self.anunciante_id,self.id].join "0"
    boleto.dias_vencimento = BOLETO_DIAS_VENCIMENTO
    boleto.data_vencimento = (self.created_at + 4).to_date
    boleto.instrucao1 = BOLETO_INSTRUCAO1
    boleto.instrucao2 = BOLETO_INSTRUCAO2
    boleto.instrucao3 = BOLETO_INSTRUCAO3
    boleto.instrucao4 = BOLETO_INSTRUCAO4
    boleto.instrucao5 = BOLETO_INSTRUCAO5
    boleto.instrucao6 = BOLETO_INSTRUCAO6
    boleto.sacado_linha1 = boleto.sacado
    boleto.sacado_linha2 = self.anunciante.endereco
    boleto.sacado_linha3 = ""
    boleto
  end


  def pagamento_efetuado?
    self.valor_a_pagar.nil? || !self.transacao.nil? || !self.pago?
  end

  def pode_reativar?
    !expirado? && self.pagamento_efetuado?
  end

  def expirado?
    Time.now.between?(self.created_at,self.expira_em) ? false : true
  end

  def pago?
    self.plano.valor > 0 ? true : false
  end

  def extensao
    self.send(self.tipo.underscore)
  end


  def salvar_banner(parametros_banner = {})
    parametros_banner ||= {}

    self.anuncio_banner ||= AnuncioBanner.new
    self.anuncio_banner.attributes = parametros_banner
    self.anuncio_banner.valid?

    AnuncioBanner.transaction do
      self.save!

      self.anuncio_banner.anuncio_id = self.id
      self.anuncio_banner.save!
    end
    true
  rescue
    false
  end


  def set_expiracao
    self.update_attribute(:expira_em,self.plano.expira_em(Date.today))
  end

  def set_tipo
    self.update_attribute(:tipo, self.plano.tipo)
  end

  def set_pag_seguro
    if self.plano.valor > 0
      self.update_attribute(:valor_a_pagar, self.plano.valor)
    else
      self.update_attribute(:data_pagamento, Date.today)
    end
  end

  def set_status
    self.update_attribute(:status, nil)
  end
  protected :set_expiracao, :set_tipo, :set_pag_seguro, :set_status
end
