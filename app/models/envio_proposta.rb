class EnvioProposta < ActiveRecord::BaseWithoutTable
  column :nome, :string
  column :email, :string
  column :telefone, :string
  column :localizacao, :string  
  column :proposta, :text
  column :anuncio_id, :integer
  
  
  validates_presence_of :nome, :email, :proposta

  def enviar_email
    if !self.valid?
      return false
    end
    
    Notificador.deliver_enviar_email_proposta(self)
  end
  
  
  @@anuncio_obj = nil
  def anuncio
    @@anuncio_obj ||= Anuncio.find(self.anuncio_id)
  end

end
