class PagSeguroTransacaoProduto < ActiveRecord::Base
  set_table_name("PagSeguroTransacoesProdutos")
  
  usar_como_dinheiro :ProdValor
  
  
  
  def self.montar_parametro(parametro,posicao)
    "#{parametro.to_s}_#{posicao}".to_symb
  end
end
