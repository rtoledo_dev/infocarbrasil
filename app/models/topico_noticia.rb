class TopicoNoticia < ActiveRecord::Base
  has_many :noticias, :order => "created_at"
  has_many :noticias_ativas, :class_name => "Noticia", :conditions => ["ativo = ?",true], :order => "created_at"
  
  validates_presence_of :descricao
  validates_uniqueness_of :descricao
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:descricao]}%"
    self.paginate :conditions => ["descricao like ?",busca], :order => "descricao", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
  
  
  def buscar_noticias_paginadas(pagina)
    self.noticias_ativas.paginate :per_page => 2, :page => pagina
  end
end
