class Banner < ActiveRecord::Base
  TIPO_IMAGEM = 0
  TIPO_FLASH = 1
  
  TIPOS = {TIPO_IMAGEM => "Imagem", TIPO_FLASH => "Flash"}

  LOCALIZACAO_TOPO = 0
  LOCALIZACAO_LATERAL = 1
  
  LOCALIZACOES = {LOCALIZACAO_TOPO => "Ao topo da página", LOCALIZACAO_LATERAL => "Ao lado da página"}
  
  validates_presence_of :tipo, :localizacao
  
  has_attached_file :arquivo, 
    :styles => { 
      :webdoor => "230x230#", :lateral => '120x60', :topo => "468x60", :thumb => "130x130#", 
      :list => "50x50#", :big => "900>", :normal => "300"
    }
  has_attached_file :arquivo_swf
  
  
  def self.pesquisa_admin(parametros_busca,pagina)
    condicoes = []
    parametros = []
    
    if parametros_busca[:ativo]
      condicoes << "ativo = ?"
      parametros << parametros_busca[:ativo]
    end
    
    if parametros_busca[:tipo]
      condicoes << "tipo = ?"
      parametros << parametros_busca[:tipo]
    end

    if parametros_busca[:localizacao]
      condicoes << "localizacao = ?"
      parametros << parametros_busca[:localizacao]
    end
    
    
    Banner.paginate :conditions => ([condicoes.join(" AND ")]+parametros),
                     :order => "created_at DESC", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end
end
