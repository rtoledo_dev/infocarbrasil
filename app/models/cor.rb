class Cor < ActiveRecord::Base
  validates_presence_of :descricao
  
  def self.pesquisa_admin(parametros,pagina)
    busca = "%#{parametros[:descricao]}%"
    self.paginate :conditions => ["cores.descricao like ?",busca],
                  :order => "cores.descricao", :per_page => MAXIMO_POR_PAGINA, :page => pagina
  end

end
