class RestritoController < FrontendController
  
  before_filter :logado?
  
  def logado?
    if session[:anunciante].nil?
      redirect_to :controller => :login
      return false
    end
    true
  end
end