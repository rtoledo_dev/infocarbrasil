class ContatoController < FrontendController
  def index
    @contato = Contato.new
  end
  
  def enviar
    return redirect_to(:action => :index) if acao_cancelar?
  
    params[:contato] ||= {}
    
    @contato = Contato.new
    @contato.attributes = params[:contato]

    respond_to do |format|
      if @contato.enviar_email
        flash[:notice] = "Email enviado com sucesso"
        
        format.html { 
          index
          render :action => "index" }
        format.xml  { render :xml => [@contato], :status => :created, :location => @contato }
      else
        flash[:error] = "Preencha os dados corretamente"
        format.html { render :action => "index" }
        format.xml  { render :xml => @contato.errors, :status => :unprocessable_entity }
      end
    end

  end
  
  def buscar_estados
    respond_to do |formato|
      formato.js
    end
  end
  
  def buscar_cidades
    respond_to do |formato|
      formato.js
    end
  end
  

end
