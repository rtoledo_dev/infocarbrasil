class FrontendController < ApplicationController
  
  before_filter :anunciante_logado, :set_buscas, :buscar_anuncios_destaques
  
  def anunciante_logado
    @anunciante_logado ||= Anunciante.find(session[:anunciante])
  rescue
    false
  end
  
  
  def set_buscas
    @busca_anuncio_veiculo    = AnuncioVeiculo.new
    @busca_anuncio_acessorio  = AnuncioAcessorio.new
    
    if request.post?
      expire_fragment(CACHE_SITE_BUSCA_TIPO_VEICULO) if params[:busca_anuncio_veiculo]
      expire_fragment(CACHE_SITE_BUSCA_TIPO_ACESSORIO) if params[:busca_anuncio_acessorio]
    end
  end
  
  
  def buscar_marcas
    @busca_anuncio_veiculo.attributes = params[:busca_anuncio_veiculo]
    respond_to do |formato|
      formato.html {render :controller => :veiculos, :action => :pesquisar}
      formato.js {render :action => "buscar_marcas.rjs"}
    end
  end
  
  def buscar_modelos
    @busca_anuncio_veiculo.attributes = params[:busca_anuncio_veiculo]
    respond_to do |formato|
      formato.html {render :controller => :veiculos, :action => :pesquisar}
      formato.js {render :action => "buscar_modelos.rjs"}
    end
  end
  
  def buscar_anuncios_destaques
    if params[:controller] != "home"
      @anuncios_veiculos_destaques = AnuncioVeiculo.all :conditions => ["planos.destaque=? and status=? AND anunciante_id IS NOT NULL", true, true],
        :include => {:anuncio => :plano}, :order => "rand()", :limit => 4
      
     
      @anuncios_acessorios_destaques = AnuncioAcessorio.all :conditions => ["planos.destaque=? and status=? AND anunciante_id IS NOT NULL", true, true],
        :include => {:anuncio => :plano}, :order => "rand()", :limit => 4    
      
    end 
  end

end
