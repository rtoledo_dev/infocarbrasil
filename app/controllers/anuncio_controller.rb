class AnuncioController < FrontendController
  def salvar_imagens(anuncio)
    params[:imagens].each do |imagem|
      AnuncioImagem.create :arquivo => imagem, :anuncio_id => anuncio.id unless imagem.blank?
    end
  end

end
