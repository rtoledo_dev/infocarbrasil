class RetornoController < FrontendController
  def index
    
    # Recebe o post da resposta automatica do pagseguro
    # nesse momento os parametros estão preenchidos com dados em variaveis em post
    if request.post?
      PagSeguroTransacao::salvar_retorno(params)
    end
  end
end
