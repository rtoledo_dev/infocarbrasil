class PedidosController < FrontendController
  
  
  def index
    @pedido = Pedido.new
    
    #pedido a partir de um anuncio, ta meio burro tem outro jeito melhor?
    if params[:id]
      @anuncio = Anuncio.find params[:id]
      params[:pedido] = @pedido
      params[:pedido][:tipo_veiculo_id] = @anuncio.anuncio_veiculo.tipo_veiculo_id
      params[:pedido][:marca_id] = @anuncio.anuncio_veiculo.marca_id
      params[:pedido][:modelo_id] = @anuncio.anuncio_veiculo.modelo_id
      params[:pedido][:cor_id] = @anuncio.anuncio_veiculo.cor_id
      params[:pedido][:combustivel] = @anuncio.anuncio_veiculo.combustivel
      params[:pedido][:kilometragem] = @anuncio.anuncio_veiculo.kilometragem
      params[:pedido][:valor] = @anuncio.anuncio_veiculo.valor
      params[:pedido][:ano] = @anuncio.anuncio_veiculo.ano
    end

  end

  def criar
    @pedido = Pedido.new(params[:pedido])
    
    if @pedido.save
      flash[:notice] = "Pedido feito com sucesso. Iremos analisá-lo e informaremos de um interesse de venda."
    else
      flash[:error] = "Erro ao criar um pedido. Preencha todos os campos corretamente."
    end
    
    render :action => :index
  end
  
  def buscar_marcas
    respond_to do |formato|
      formato.js
    end
  end
  
  
  def buscar_modelos
    respond_to do |formato|
      formato.js
    end
  end

end
