class Admin::OpcionaisController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @opcional = Opcional.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @opcional }
    end    
  end

  def editar
    @opcional = Opcional.find(params[:id])
  end
  
  def criar
    return redirect_to(:action => :index) if acao_cancelar?

    params[:opcional][:tipo_veiculo_ids] ||= []
    
    @opcional = Opcional.new params[:opcional]

    respond_to do |format|
      if @opcional.save
        sucesso_criar
        format.html { redirecionamento(@opcional) }
        format.xml  { render :xml => @opcional, :status => :created, :location => @opcional }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @opcional.errors, :status => :unprocessable_entity }
      end
    end
    
  end
  
  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    params[:opcional][:tipo_veiculo_ids] ||= []
    
    @opcional = Opcional.find(params[:id])
    
    respond_to do |format|
      if @opcional.update_attributes(params[:opcional])
        sucesso_atualizar
        format.html { redirecionamento(@opcional) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @opcional.errors, :status => :unprocessable_entity }
      end
    end    
  end

  def apagar
    @opcional = Opcional.find(params[:id])
    @opcional.destroy

    pesquisar
  end
  
  def pesquisar
    params[:page] ||= 1
    params[:opcional] ||= {}
    @opcionais = Opcional::pesquisa_admin(params[:opcional],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @opcionais }
    end
  end

end
