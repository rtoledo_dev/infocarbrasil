class Admin::PlanosController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @plano = Plano.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @plano }
    end    
  end

  def editar
    @plano = Plano.find(params[:id])
  end
  
  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @plano = Plano.new params[:plano]

    respond_to do |format|
      if @plano.save
        sucesso_criar
        format.html { redirecionamento(@plano) }
        format.xml  { render :xml => @plano, :status => :created, :location => @plano }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @plano.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @plano = Plano.find(params[:id])

    respond_to do |format|
      if @plano.update_attributes(params[:plano])
        sucesso_atualizar
        format.html { redirecionamento(@plano) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @plano.errors, :status => :unprocessable_entity }
      end
    end    
  end
  
  def apagar
    @plano = Plano.find(params[:id])
    @plano.destroy

    pesquisar
  end  
  
  def pesquisar
    params[:page] ||= 1
    params[:plano] ||= {}
    @planos = Plano::pesquisa_admin(params[:plano],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @planos }
    end
  end

end
