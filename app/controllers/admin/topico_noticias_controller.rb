class Admin::TopicoNoticiasController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @topico_noticia = TopicoNoticia.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @topico_noticia }
    end
  end

  def editar
    @topico_noticia = TopicoNoticia.find(params[:id])
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @topico_noticia = TopicoNoticia.new params[:topico_noticia]

    respond_to do |format|
      if @topico_noticia.save
        sucesso_criar
        format.html { redirecionamento(@topico_noticia) }
        format.xml  { render :xml => @topico_noticia, :status => :created, :location => @topico_noticia }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @topico_noticia.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @topico_noticia = TopicoNoticia.find(params[:id])

    respond_to do |format|
      if @topico_noticia.update_attributes(params[:topico_noticia])
        sucesso_atualizar
        format.html { redirecionamento(@topico_noticia) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @topico_noticia.errors, :status => :unprocessable_entity }
      end
    end
  end

  def apagar
    @topico_noticia = TopicoNoticia.find(params[:id])
    @topico_noticia.destroy

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:topico_noticia] ||= {}
    @topico_noticias = TopicoNoticia::pesquisa_admin(params[:topico_noticia],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @topico_noticias }
    end
  end
end
