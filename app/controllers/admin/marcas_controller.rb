class Admin::MarcasController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @marca = Marca.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @marca }
    end
  end

  def editar
    @marca = Marca.find(params[:id])
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @marca = Marca.new params[:marca]

    respond_to do |format|
      if @marca.save
        sucesso_criar
        format.html { redirecionamento(@marca) }
        format.xml  { render :xml => @marca, :status => :created, :location => @marca }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @marca.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @marca = Marca.find(params[:id])

    respond_to do |format|
      if @marca.update_attributes(params[:marca])
        sucesso_atualizar
        format.html { redirecionamento(@marca) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @marca.errors, :status => :unprocessable_entity }
      end
    end
  end

  def apagar
    @marca = Marca.find(params[:id])
    @marca.destroy

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:marca] ||= {}
    @marcas = Marca::pesquisa_admin(params[:marca],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @marcas }
    end
  end

end
