class Admin::NoticiasController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @noticia = Noticia.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @noticia }
    end
  end

  def editar
    @noticia = Noticia.find(params[:id])
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @noticia = Noticia.new params[:noticia]

    respond_to do |format|
      if @noticia.save
        expire_fragment(CACHE_HOME_NOTICIAS)
        sucesso_criar
        format.html { redirecionamento(@noticia) }
        format.xml  { render :xml => @noticia, :status => :created, :location => @noticia }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @noticia.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @noticia = Noticia.find(params[:id])

    respond_to do |format|
      if @noticia.update_attributes(params[:noticia])
        expire_fragment(CACHE_HOME_NOTICIAS)
        sucesso_atualizar
        format.html { redirecionamento(@noticia) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @noticia.errors, :status => :unprocessable_entity }
      end
    end
  end

  def apagar
    @noticia = Noticia.find(params[:id])
    @noticia.destroy
    expire_fragment(CACHE_HOME_NOTICIAS)

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:noticia] ||= {}
    @noticias = Noticia::pesquisa_admin(params[:noticia],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @noticias }
    end
  end
end
