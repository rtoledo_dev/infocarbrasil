class Admin::PedidosController < Admin::RestritoController
  def index
    pesquisar
  end

  def editar
    @pedido = Pedido.find(params[:id])
  end
  
  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @pedido = Pedido.find(params[:id])

    respond_to do |format|
      if @pedido.update_attributes(params[:pedido])
        sucesso_atualizar
        format.html { redirecionamento(@pedido) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @pedido.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def apagar
    @pedido = Pedido.find(params[:id])
    @pedido.destroy

    pesquisar
  end  
  
  def pesquisar
    params[:page] ||= 1
    params[:pedido] ||= {}
    @pedidos = Pedido::pesquisa_admin(params[:pedido],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @pedidoes }
    end
    
  end

end
