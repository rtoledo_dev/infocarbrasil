class Admin::TipoVeiculosController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @tipo_veiculo = TipoVeiculo.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @tipo_veiculo }
    end
  end

  def editar
    @tipo_veiculo = TipoVeiculo.find(params[:id])
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @tipo_veiculo = TipoVeiculo.new params[:tipo_veiculo]

    respond_to do |format|
      if @tipo_veiculo.save
        expire_cache(CACHE_SITE_BUSCA_TIPO_VEICULO)
        sucesso_criar
        format.html { redirecionamento(@tipo_veiculo) }
        format.xml  { render :xml => @tipo_veiculo, :status => :created, :location => @tipo_veiculo }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @tipo_veiculo.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @tipo_veiculo = TipoVeiculo.find(params[:id])

    respond_to do |format|
      if @tipo_veiculo.update_attributes(params[:tipo_veiculo])
        expire_cache(CACHE_SITE_BUSCA_TIPO_VEICULO)
        sucesso_atualizar
        format.html { redirecionamento(@tipo_veiculo) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @tipo_veiculo.errors, :status => :unprocessable_entity }
      end
    end
  end

  def apagar
    @tipo_veiculo = TipoVeiculo.find(params[:id])
    @tipo_veiculo.destroy
    expire_cache(CACHE_SITE_BUSCA_TIPO_VEICULO)

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:tipo_veiculo] ||= {}
    @tipo_veiculos = TipoVeiculo::pesquisa_admin(params[:tipo_veiculo],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @tipo_veiculos }
    end
  end

end
