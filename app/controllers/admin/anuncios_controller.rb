class Admin::AnunciosController < Admin::RestritoController
  def index
    pesquisar
  end
  
  def visualizar
    @anuncio = Anuncio.find(params[:id])
    
    @anuncio_veiculo = @anuncio.anuncio_veiculo
    #@anuncio_banner = @anuncio.anuncio_banner
    @anuncio_acessorio = @anuncio.anuncio_acessorio
  end
  
  def pesquisar
    params[:page] ||= 1
    params[:anuncio] ||= {}
    
    @anunciantes = Anunciante::pesquisa_anuncios(params[:anuncio],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @anuncios }
    end    
  end
  
  def pendentes
    pesquisar_pendentes
  end

  def pesquisar_pendentes
    params[:page] ||= 1
    params[:anuncio] ||= {}
    
    params[:anuncio][:status] = "NULL"
    @anunciantes = Anunciante::pesquisa_anuncios(params[:anuncio],params[:page])
    respond_to do |formato|
      formato.html { render :action => "pendentes" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @anuncios }
    end    
  end
  def expirar
    @resultado_expiracao = Anuncio.expirar(params[:id])
    @anuncio = params[:id]
    respond_to do |formato|
      formato.js
    end
  end
  
  
  def reativar
    @anuncio = Anuncio.find params[:id]
    if @anuncio.pago?
      @resultado_reativacao = Anuncio.efetuar_pagamento(params[:id])
    else
      @resultado_reativacao = Anuncio.reativar(params[:id])
    end  
    respond_to do |formato|
      formato.js
    end
  end
  
end
