class Admin::TipoAcessoriosController < Admin::RestritoController
  
  def index
    pesquisar
  end

  def novo
    @tipo_acessorio = TipoAcessorio.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @tipo_acessorio }
    end
  end

  def editar
    @tipo_acessorio = TipoAcessorio.find(params[:id])
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @tipo_acessorio = TipoAcessorio.new params[:tipo_acessorio]

    respond_to do |format|
      if @tipo_acessorio.save
        expire_cache(CACHE_SITE_BUSCA_TIPO_ACESSORIO)
        sucesso_criar
        format.html { redirecionamento(@tipo_acessorio) }
        format.xml  { render :xml => @tipo_acessorio, :status => :created, :location => @tipo_acessorio }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @tipo_acessorio.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @tipo_acessorio = TipoAcessorio.find(params[:id])

    respond_to do |format|
      if @tipo_acessorio.update_attributes(params[:tipo_acessorio])
        expire_cache(CACHE_SITE_BUSCA_TIPO_ACESSORIO)
        sucesso_atualizar
        format.html { redirecionamento(@tipo_acessorio) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @tipo_acessorio.errors, :status => :unprocessable_entity }
      end
    end
  end

  def apagar
    @tipo_acessorio = TipoAcessorio.find(params[:id])
    @tipo_acessorio.destroy
    expire_cache(CACHE_SITE_BUSCA_TIPO_ACESSORIO)

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:tipo_acessorio] ||= {}
    @tipo_acessorios = TipoAcessorio::pesquisa_admin(params[:tipo_acessorio],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @tipo_acessorios }
    end
  end
end
