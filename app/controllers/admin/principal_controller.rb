class Admin::PrincipalController < Admin::RestritoController
  def index
    @anunciantes = Anunciante.all(:order => "created_at DESC", :limit => MAXIMO_POR_PAGINA)

    @anuncios_mais_vistos = Anuncio.all(:joins => :anunciante,
                                        :conditions => ["status = ? AND anunciante_id IS NOT NULL",true],
                                        :order => "visitas DESC",
                                        :limit => MAXIMO_POR_PAGINA)
    @anuncios_expirados = Anuncio.all(:joins => :anunciante,
                                      :conditions => ["status = ? AND anunciante_id IS NOT NULL",false],
                                      :order => "updated_at DESC",
                                      :limit => MAXIMO_POR_PAGINA)
    @anuncios_a_expirar = Anuncio.all(:joins => :anunciante,
                                      :conditions => ["status = ? AND anunciante_id IS NOT NULL",true],
                                      :order => "created_at",
                                      :limit => MAXIMO_POR_PAGINA)
  end

end
