class Admin::LoginController < ApplicationController
  
  before_filter :redireciona_se_logado
  
  def redireciona_se_logado
    if @administrador_logado
      redirect_to :controller => :principal
      return
    end
  end
  
  def index
    @administrador = Administrador.new
  end
  
  def entrar
    @administrador = Administrador.new(params[:administrador])
    if @administrador.login?
      session[:administrador] = @administrador.id
      redirect_to :controller => :principal
      return
    else
      flash[:error] = "Usuário/Senha incorretos"
      render :action => :index
    end
  end
  
  def sair
    session[:administrador] = nil
    index
    render :action => :index
  end
  
end
