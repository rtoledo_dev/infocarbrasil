class Admin::RestritoController < ApplicationController
  
  before_filter :logado?, :administrador_logado
  
  def logado?
    if session[:administrador].nil?
      redirect_to :controller => :login
      return false
    end
    true
  end
  
  
  def administrador_logado
    @administrador_logado = Administrador.find(session[:administrador])
  rescue
    nil
  end
end