class Admin::CoresController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @cor = Cor.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @cor }
    end
    
  end

  def editar
    @cor = Cor.find(params[:id])
  end
  
  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @cor = Cor.new params[:cor]

    respond_to do |format|
      if @cor.save
        sucesso_criar
        format.html { redirecionamento(@cor) }
        format.xml  { render :xml => @cor, :status => :created, :location => @cor }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @cor.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @cor = Cor.find(params[:id])

    respond_to do |format|
      if @cor.update_attributes(params[:cor])
        sucesso_atualizar
        format.html { redirecionamento(@cor) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @cor.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def apagar
    @cor = Cor.find(params[:id])
    @cor.destroy

    pesquisar
  end  
  
  def pesquisar
    params[:page] ||= 1
    params[:cor] ||= {}
    @cores = Cor::pesquisa_admin(params[:cor],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @cores }
    end
    
  end
end
