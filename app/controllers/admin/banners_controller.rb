class Admin::BannersController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @anuncio = Anuncio.new
    @anuncio_banner = AnuncioBanner.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @anuncio }
    end
  end

  def editar
    @anuncio        = Anuncio.find(params[:id])
    @anuncio_banner = @anuncio.anuncio_banner
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @anuncio = Anuncio.new params[:anuncio]
    @anuncio.validar_anuncio = true

    respond_to do |format|
      if @anuncio.salvar_banner(params[:anuncio_banner])
        @anuncio_banner = @anuncio.anuncio_banner
        
        sucesso_criar
        format.html { redirecionamento(@anuncio) }
        format.xml  { render :xml => @anuncio, :status => :created, :location => @anuncio }
      else
        @anuncio_banner = @anuncio.anuncio_banner
        
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @anuncio.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?

    @anuncio = Anuncio.find(params[:id])
    @anuncio.attributes = params[:anuncio]
    @anuncio.validar_anuncio = true

    respond_to do |format|
      if @anuncio.salvar_banner(params[:anuncio_banner])
        @anuncio_banner = @anuncio.anuncio_banner
        
        sucesso_atualizar
        format.html { redirecionamento(@anuncio) }
        format.xml  { head :ok }
      else
        @anuncio_banner = @anuncio.anuncio_banner
        
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @anuncio.errors, :status => :unprocessable_entity }
      end
    end
  end
  
  def apagar
    @anuncio = Anuncio.find(params[:id])
    @anuncio.destroy

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:anuncio_banner] ||= {}
    @anuncios = AnuncioBanner::pesquisa_admin(params[:anuncio_banner],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @anuncios }
    end
  end

end
