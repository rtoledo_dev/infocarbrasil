class Admin::ModelosController < Admin::RestritoController
  def index
    pesquisar
  end

  def novo
    @modelo = Modelo.new
    
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @modelo }
    end
  end

  def editar
    @modelo = Modelo.find(params[:id])
  end

  def criar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @modelo = Modelo.new params[:modelo]

    respond_to do |format|
      if @modelo.save
        sucesso_criar
        format.html { redirecionamento(@modelo) }
        format.xml  { render :xml => @modelo, :status => :created, :location => @modelo }
      else
        erro_criar
        format.html { render :action => "novo" }
        format.xml  { render :xml => @modelo.errors, :status => :unprocessable_entity }
      end
    end
  end

  def atualizar
    return redirect_to(:action => :index) if acao_cancelar?
    
    @modelo = Modelo.find(params[:id])

    respond_to do |format|
      if @modelo.update_attributes(params[:modelo])
        sucesso_atualizar
        format.html { redirecionamento(@modelo) }
        format.xml  { head :ok }
      else
        erro_atualizar
        format.html { render :action => "editar" }
        format.xml  { render :xml => @modelo.errors, :status => :unprocessable_entity }
      end
    end
  end

  def apagar
    @modelo = Modelo.find(params[:id])
    @modelo.destroy

    pesquisar
  end

  def pesquisar
    params[:page] ||= 1
    params[:modelo] ||= {}
    @modelos = Modelo::pesquisa_admin(params[:modelo],params[:page])
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @modelos }
    end
  end
end
