class PainelAnuncianteController < RestritoController
  
  
  def index
    salvar_anuncio_pendente
    pesquisar
  end
  
  def anuncio
    @anuncio = Anuncio.find params[:id]
  end
  
  def interesses
    @pedidos = Pedido.buscar_por_anuncios(@anunciante_logado.pesquisar_anuncios_veiculos)
  end
  
  def comentar
  end
  
  def apagar_comentario
  end
  
  def atualizar_anuncio
  end
  
  def pesquisar
    params[:page] ||= 1
    params[:anuncio] ||={}
    
    params[:anuncio][:anunciante] = @anunciante_logado.id
    
    @anuncios = Anuncio::anunciante_anuncios(params[:anuncio], params[:page])
    #@anuncio_salvo = @anuncios.first
    
    respond_to do |formato|
      formato.html { render :action => "index" }
      formato.js   { render :action => "pesquisar.rjs" }
      formato.xml  { render :xml => @anuncios }
    end
    
  end
  
  
  def salvar_anuncio_pendente
    unless session[:anuncio_temporario].nil?
      @anuncio_salvo = Anuncio.find(session[:anuncio_temporario]) rescue nil
      unless @anuncio_salvo.nil?
        @anuncio_salvo.update_attribute(:anunciante_id,@anunciante_logado.id)
        flash[:notice] = "Anúncio incluido com sucesso"
        @realizar_compra = true
      end
      session[:anuncio_temporario] = nil
    end
  end
  
  def enviar_a_pag_seguro
    
  end
  
  
  def expirar
    @resultado_expiracao = Anuncio.expirar(params[:id])
    @anuncio = params[:id]
    respond_to do |formato|
      formato.js
    end
  end
  
  
  def reativar
    @anuncio = params[:id]
    @resultado_reativacao = Anuncio.reativar(params[:id])
    respond_to do |formato|
      formato.js
    end
  end
  
  
  
  def gerar_boleto
    @anuncio = Anuncio.find(params[:id])
    
    unless @anunciante_logado.anuncios.include?(@anuncio)
      return redirecionar_index("Você não tem permissão neste anúncio")
    end
    
    @boleto = @anuncio.gerar_boleto
    send_data @boleto.boleto_pdf, :filename => "boleto_#{@anuncio.created_at.to_date.to_s(:br)}.pdf"
    
  rescue => erro
    redirecionar_index "Erro ao gerar o boleto", erro.message
  end
  
  
  
  def redirecionar_index(mensagem,erro = nil)
    mensagem << ": #{erro}" unless erro.nil?
    flash[:error] = mensagem
    index
  end
  
end
