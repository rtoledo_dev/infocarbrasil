class LoginController < FrontendController
  before_filter :redirecionar_se_logado
    
  def redirecionar_se_logado
    if @anunciante_logado && action_name != "deslogar"
      redirect_to :controller => :painel_anunciante
      return
    end
  end
  
  def index
    session[:anunciante] = nil
    @anunciante = Anunciante.new
    @anunciante.tipo_login = Anunciante::ANUNCIANTE
  end
  
  def logar
    @anunciante = Anunciante.new(params[:anunciante])
    if @anunciante.login?
      session[:anunciante] = @anunciante.id
      redirect_to :controller => :painel_anunciante
      return
    else
      flash[:error] = "Usuário/Senha incorretos"
      render :action => :index
    end
  end

  def deslogar
    session[:anunciante] = nil
    redirect_to :controller => :home
  end

end
