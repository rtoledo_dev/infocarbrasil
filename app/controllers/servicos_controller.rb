class ServicosController < FrontendController
  def index
  end



  def ver
    params[:page] = 1 if params[:page].to_i <= 0
    @tipo_loja = TipoLoja.find(params[:id])
    @lojas = @tipo_loja.lojas_paginadas(params[:page])
  rescue
    redirect_to :action => :index
  end

end
