class CadastroController < FrontendController
  
  def redirecionar_se_logado
    if @anunciante_logado
      return
    end
  end
  
  def index
    redirect_to :action => :editar if @anunciante_logado
    
    session[:tipo_anunciante] = Anunciante::TIPOS.include?(params[:tipo].to_i) ? params[:tipo].to_i : Anunciante::ANUNCIANTE
    @anunciante = Anunciante.new
    @anunciante.attributes = params[:anunciante] if request.get?
  end

  def criar
    if @anunciante_logado
      redirect_to :controller => :painel_anunciante
      return
    end
    
    params[:loja][:tipo_loja_ids] ||= [] if params[:loja]
    @anunciante = Anunciante.new params[:anunciante]
    @anunciante.ativo = true
    @anunciante.validar_senha = true

    if @anunciante.salvar(session[:tipo_anunciante],params[:loja])
      if @anunciante.loja
        expire_cache(CACHE_HOME_TIPO_LOJAS)
        expire_cache(CACHE_SITE_BUSCA_LOJAS)
      end
      salvar_imagens(@anunciante.loja)
      
      
      session[:anunciante] = @anunciante.id
      anunciante_logado
      
      redirect_to :controller => :painel_anunciante
      return
      
    else
      @loja = @anunciante.loja
      flash[:error] = "erro ao cadastrar"
      render :action => :index
    end
  end



  def editar
    @anunciante = @anunciante_logado
    @loja       = @anunciante.loja
    
    session[:tipo_anunciante] = @anunciante.tipo
    
    
    render :action => :index
  end

  def atualizar
    params[:loja][:tipo_loja_ids] ||= [] if params[:loja]
    
    @anunciante = @anunciante_logado
    session[:tipo_anunciante] = @anunciante.tipo
    @anunciante.attributes = params[:anunciante]

    if @anunciante.salvar(session[:tipo_anunciante],params[:loja])
      if @anunciante.loja
        expire_cache(CACHE_HOME_TIPO_LOJAS)
        expire_cache(CACHE_SITE_BUSCA_LOJAS)
      end
      salvar_imagens(@anunciante.loja)
      
      flash[:notice] = "Salvou"
    else
      flash[:error] = "erro ao atualizar"
    end
    
    @loja = @anunciante.loja
    
    render :action => :index
  end



  def buscar_cidades
    respond_to do |formato|
      formato.js
    end
  end

  def buscar_estados
    respond_to do |formato|
      formato.js
    end
  end
  
  def salvar_imagens(loja)
    if loja
      params[:imagens].each do |imagem|
        FotoLoja.create :arquivo => imagem, :loja_id => loja.id unless imagem.blank?
      end
    end
  end

end
