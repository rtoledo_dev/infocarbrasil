namespace :agencerails do
  desc 'Copiar arquivos gerais'
  task :instalar do
    require "config/environment"
    require 'fileutils'
    
    directory = File.join(RAILS_ROOT, '/vendor/plugins/agence_rails/')
    require "#{directory}lib/agence_rails_install"
      
    AgenceRailsInstall.install(true) 
         
  end
end

