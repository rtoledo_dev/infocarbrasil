MAXIMO_POR_PAGINA = 10


CACHE_HOME_TIPO_LOJAS = "cache_home_tipo_lojas"
CACHE_HOME_NOTICIAS = "cache_home_noticias"
CACHE_SITE_BUSCA_TIPO_VEICULO = "cache_site_busca_tipo_veiculo"
CACHE_SITE_BUSCA_TIPO_ACESSORIO = "cache_site_busca_tipo_acessorio"
CACHE_SITE_BUSCA_LOJAS = "cache_site_busca_lojas"



BOLETO_CEDENTE = "Luiz Alexandre"
BOLETO_DOCUMENTO_CEDENTE = "123123123"
BOLETO_AGENCIA = "4042"
BOLETO_CONTA_CORRENTE = "61900"
BOLETO_CONVENIO = "1238798"
BOLETO_DIAS_VENCIMENTO = 5
BOLETO_INSTRUCAO1 = "Pagável na rede bancária até a data de vencimento."
BOLETO_INSTRUCAO2 = "Juros de mora de 2.0% mensal(R$ 0,09 ao dia)"
BOLETO_INSTRUCAO3 = ""
BOLETO_INSTRUCAO4 = ""
BOLETO_INSTRUCAO5 = "Após vencimento pagável somente nas agências do Banco do Brasil"
BOLETO_INSTRUCAO6 = "ACRESCER R$ 4,00 REFERENTE AO BOLETO BANCÁRIO"