module ActionPackController
    def acao_cancelar?
      params[:botao_clicado] == "cancelar"
    rescue
      false
    end
    
    def acao_salvar_listar?
      params[:botao_clicado] == "salvar_listar"
    rescue
      false
    end
    
    def acao_salvar_novo?
      params[:botao_clicado] == "salvar_novo"
    rescue
      false
    end
    
    
    
    
    # -- Metodos Protegidos
    def redirecionamento(modelo)
      if acao_salvar_listar?
        redirect_to :action => :index
      elsif acao_salvar_novo?
        redirect_to :action => :novo
      else
        redirect_to :action => :editar, :id => modelo
      end
      return
    end
    
    
    def registro_invalido
      redirect_to :action => :index
      return
    end
    
    def erro_criar
      flash[:error] = "Falha ao criar o registro"
    end
    
    def erro_atualizar
      flash[:error] = "Falha ao atualizar o registro"
    end
    
    def sucesso_criar
      flash[:notice] = "Registro criado com sucesso"
    end
    
    def sucesso_atualizar
      flash[:notice] = "Registro atualizado com sucesso"
    end
  
end
ActionController::Base.send(:include, ActionPackController)
