module AgenceHelper

  def mascara_telefone(telefone)
    telefone = telefone.to_s
    html  = '('
    html << telefone[0,2]
    html << ')'
    html << ' '
    html << telefone[2,4]
    html << '-'
    html << telefone[6,4]
    html
    
  rescue
    telefone
  end
  
  
  # Mensagens na tela
  def flash_message
    mensagens = ''
    [:notice, :info, :warning, :error].each {|type|
      if flash[type]
        mensagens << "<div class=\"alert_message #{type}\">"
        mensagens << "#{flash[type]}"
        mensagens << "<div class=\"btn_close\">#{link_to(image_tag("icons/exit2.gif", :alt => "Fechar mensagem", :title => "Fechar mensagem"), "javascript:;", :onclick => "Effect.Fade($(this).up(1));")}</div>"
        mensagens << "</div>"
      end
    }
    
    mensagens
  end
  
  
  # Paginacao sem AJAX
  # <tt>*objeto</tt> Deve ser um objeto gerado pelo plugin will_paginate
  # <tt>*parametros</tt> Deve ser um +Hash+ com os parametros
  def paginacao(objeto,parametros = {})
    will_paginate(objeto,
                  :params => parametros,
                  :prev_label => image_tag("seta_esq.jpg", :border => 0), :next_label => image_tag("seta_dir.jpg", :border => 0))
  end
  
   # Paginacao com AJAX
  # <tt>*objeto</tt>: Deve ser um objeto gerado pelo plugin will_paginate
  # <tt>*parametros</tt>: Deve ser um +Hash+, com os parametros
  # Exemplos:
  #     paginacao_ajax @materiais, {:with => "Form.serialize('form_pesquisa')"}
  #     paginacao_ajax @materiais, {:url => {:action => "pesquisa", :page => params[:page]}}
  def paginacao_ajax(objeto, ajax_parametros = {})
    will_paginate(objeto,
                  :renderer => 'RemoteLinkRenderer',
                  :remote => ajax_parametros,
                  :prev_label => image_tag("seta_esq.jpg", :border => 0), :next_label => image_tag("seta_dir.jpg", :border => 0))
  end
  
  
  
  def montar_menu_rastro(itens_rastro)
    menu_rastro = "Sistema &gt; "
    if itens_rastro.size > 0
      for i in 0..(itens_rastro.size - 1) do
        if (i + 1) != itens_rastro.size
          menu_rastro << "#{itens_rastro[i]} &gt; "
        else
          menu_rastro << "<span class=\"ultimo\">#{itens_rastro[i]}</span>"
        end
      end
    end
    menu_rastro
    
  rescue
    menu_rastro
  end
  
  
  
  
  # Botões padronizados na tela de Edição e Novo registro
  # <tt>botoes</tt>: +NilClass+, caso este parametro seja nulo, sera considerado o padrão com os botões, salvar, salvar novo, salvar e listar, cancelar
  def montar_botoes_salvar(botoes = nil)
    botoes ||= {:salvar => true,:salvar_novo => true, :salvar_listar => true,:cancelar => true}
    render :partial => 'shared/botoes_salvar', :locals => {:botoes => botoes}
  end
  
  

  def boolean_for_human(value)
    if value == true || value.to_s == "t"
      'Sim'
    else
      'Não'
    end
  end
  
end


ActionView::Base.send :include, AgenceHelper