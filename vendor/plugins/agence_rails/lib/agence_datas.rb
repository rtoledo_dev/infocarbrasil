[Time, Date, DateTime].each do |clazz|
  eval "#{clazz}::MONTHNAMES = [nil] + %w(Janeiro Fevereiro Março Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro)"
  eval "#{clazz}::DAYNAMES = %w(Domingo Segunda-Feira Terça-Feira Quarta-Feira Quinta-Feira Sexta-Feira Sábado)"
  eval "#{clazz}::ABBR_MONTHNAMES = [nil] + %w(Jan Fev Mar Abr Mai Jun Jul Ago Set Out Nov Dez)"
  eval "#{clazz}::ABBR_DAYNAMES = %w(Dom Seg Ter Qua Qui Sex Sab)"
end



module ActiveSupport::CoreExtensions::String::Conversions
  # Cria a data no padrao brasileiro e permanece aceitando no formato tradicional.
  #
  # Exemplo:
  # "27/09/2007".to_date
  def to_date
    if /(\d{1,2})\W(\d{1,2})\W(\d{4})/ =~ self
      ::Date.new($3.to_i, $2.to_i, $1.to_i)
    elsif /(\d{1,2})\W(\d{1,2})\W(\d{4})\W(\d{1,2}):(\d{1,2})/ =~ self
      ::Date.new($3.to_i, $2.to_i, $1.to_i)
    elsif /(\d{1,2})\W(\d{1,2})\W(\d{4})\W(\d{1,2}):(\d{1,2}):(\d{1,2})/ =~ self
      ::Date.new($3.to_i, $2.to_i, $1.to_i)
    else
      ::Date.new(*ParseDate.parsedate(self)[0..2])
    end
  rescue
    nil
  end
  
  # Cria a data no padrao brasileiro e permanece aceitando no formato tradicional.
  #
  # Exemplo:
  # "27/09/2007 00:00:00".to_date
  def to_datetime
    if /(\d{1,2})\W(\d{1,2})\W(\d{4})\W(\d{1,2}):(\d{1,2}):(\d{1,2})/ =~ self
      ::DateTime.new($3.to_i, $2.to_i, $1.to_i)
    elsif /(\d{1,2})\W(\d{1,2})\W(\d{4})\W(\d{1,2}):(\d{1,2})/ =~ self
      ::DateTime.new($3.to_i, $2.to_i, $1.to_i, $4.to_i, $5.to_i)
    elsif /(\d{1,2})\W(\d{1,2})\W(\d{4})\W(\d{1,2}):(\d{1,2}):(\d{1,2})/ =~ self
      ::DateTime.new($3.to_i, $2.to_i, $1.to_i, $4.to_i, $5.to_i, $5.to_i)
    else
      ::DateTime.new(*ParseDate.parsedate(self)[0..2])
    end
  rescue
    nil
  end
end

{:br_s => '%d de %B de %Y',:br => '%d/%m/%Y'}.each do |tipo,formato|
  ActiveSupport::CoreExtensions::Date::Conversions::DATE_FORMATS[tipo] = formato
end

{:br_s => '%d de %B de %Y às %H:%M:%S',:br => '%d/%m/%Y %H:%M:%S'}.each do |tipo,formato|
  ActiveSupport::CoreExtensions::Time::Conversions::DATE_FORMATS[tipo] = formato
end