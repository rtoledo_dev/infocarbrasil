module ActiveRecordCalendario
  
  
  # Passando um atributo, metodos sao criados com o sufixo _br para ter o formato em portugues Brasil
  # Utilizacao
  #
  #   calendario_em :dt_tarefa
  #   calendario_em :hr_agendada, :hora => true
  def calendario_em(atributo,opcoes = {})
    opcoes_default = {:hora => false}
    
    
    opcoes = opcoes_default.merge opcoes
    
    define_method "#{atributo}_br" do
      read_attribute(atributo).to_s(:br)
    end
    
    define_method "#{atributo}_br=" do |valor|
      if opcoes[:hora]
        write_attribute(atributo,valor.to_datetime)
      else
        write_attribute(atributo,valor.to_date)
      end
    end
    
  end
end

class ActiveRecord::Base
  extend ActiveRecordCalendario
end