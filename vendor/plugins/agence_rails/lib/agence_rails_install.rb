require 'fileutils'

module AgenceRailsInstall
  PLUGIN_NOME = 'agence_rails'
  PLUGIN_CAMINHO = File.join(RAILS_ROOT, "/vendor/plugins/#{PLUGIN_NOME}")
  PLUGIN_CAMINHO_PUBLICO = File.join(PLUGIN_CAMINHO, "/public")
  PLUGIN_CAMINHO_VIEWS = File.join(PLUGIN_CAMINHO, "/app/views")
  
  CAMINHO_PUBLIC = File.join(RAILS_ROOT, "/public/")
  CAMINHO_VIEWS = File.join(RAILS_ROOT, "/app/views/")
  
  
  def AgenceRailsInstall.recursive_copy(options)
    source = options[:source]
    dest = options[:dest]
    logging = options[:logging].nil? ? true : options[:logging]
    
    Dir.foreach(source) do |entry|
      next if entry =~ /^\./
      if File.directory?(File.join(source, entry))
        unless File.exist?(File.join(dest, entry))
          if logging
            puts "############## Criando diretório: #{entry}..."
          end
          FileUtils.mkdir File.join(dest, entry)#, :noop => true#, :verbose => true
        end
        recursive_copy(:source => File.join(source, entry), 
                       :dest => File.join(dest, entry), 
                       :logging => logging)
      else
        if logging
          puts "  Copiando arquivo #{dest}/#{entry}..."
        end
        FileUtils.cp File.join(source, entry), File.join(dest, entry)#, :noop => true#, :verbose => true
      end
    end
  end
  
  
  def AgenceRailsInstall.install(log) 
    FileUtils.mkdir(CAMINHO_VIEWS) unless File.exist?(CAMINHO_VIEWS)
    
    recursive_copy(:source => PLUGIN_CAMINHO_VIEWS, :dest => CAMINHO_VIEWS, :logging => log)    
    recursive_copy(:source => PLUGIN_CAMINHO_PUBLICO, :dest => CAMINHO_PUBLIC, :logging => log)    
  end
  
end