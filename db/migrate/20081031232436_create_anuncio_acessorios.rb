class CreateAnuncioAcessorios < ActiveRecord::Migration
  def self.up
    create_table :anuncio_acessorios do |t|
      t.references :anuncio
      t.references :tipo_acessorio
      t.string :tipo_acessorio_outro
      t.decimal :valor

      t.timestamps
    end
  end

  def self.down
    drop_table :anuncio_acessorios
  end
end
