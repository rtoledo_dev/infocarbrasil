class CreateOpcionais < ActiveRecord::Migration
  def self.up
    create_table :opcionais do |t|
      t.string :descricao

      t.timestamps
    end
  end

  def self.down
    drop_table :opcionais
  end
end
