class CreateAnunciantes < ActiveRecord::Migration
  def self.up
    create_table :anunciantes do |t|
      t.string :nome
      t.string :email
      t.string :cpf
      t.string :senha
      t.string :endereco
      t.references :cidade
      t.string :telefone
      t.string :celular
      t.string :email_secundario
      t.integer :tipo
      t.boolean :ativo

      t.timestamps
    end
  end

  def self.down
    drop_table :anunciantes
  end
end
