class CreatePedidos < ActiveRecord::Migration
  def self.up
    create_table :pedidos do |t|
      t.string :interessado
      t.string :email
      t.string :telefone, :null => true, :default => nil
      t.string :celular, :null => true, :default => nil
      t.text :descricao
      t.references :modelo, :null => true, :default => nil
      t.references :marca, :null => true, :default => nil
      t.references :tipo_veiculo, :null => true, :default => nil
      t.references :cor, :null => true, :default => nil
      t.integer :ano
      t.decimal :valor
      t.integer :combustivel
      t.integer :kilometragem
      t.boolean :analisado

      t.timestamps
    end
  end

  def self.down
    drop_table :pedidos
  end
end
