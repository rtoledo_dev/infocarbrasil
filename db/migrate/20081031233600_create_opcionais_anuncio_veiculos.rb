class CreateOpcionaisAnuncioVeiculos < ActiveRecord::Migration
  def self.up
    create_table :opcionais_anuncio_veiculos, :id => false do |t|
      t.references :opcional
      t.references :anuncio_veiculo
    end
  end

  def self.down
    drop_table :opcionais_anuncio_veiculos
  end
end
