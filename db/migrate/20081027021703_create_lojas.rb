class CreateLojas < ActiveRecord::Migration
  def self.up
    create_table :lojas do |t|
      t.references :anunciante
      t.string :cnpj
      t.string :razao_social
      t.string :site
      t.string :mapa
      t.string :telefone_responsavel
      t.string :celular_responsavel

      t.timestamps
    end
  end

  def self.down
    drop_table :lojas
  end
end
