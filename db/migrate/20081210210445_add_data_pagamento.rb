class AddDataPagamento < ActiveRecord::Migration
  def self.up
    change_table(:anuncios) do |t|
      t.date :data_pagamento
    end
  end

  def self.down
    t.remove :data_pagamento
  end
end
