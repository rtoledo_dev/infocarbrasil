class CreateLojasTipoLojas < ActiveRecord::Migration
  def self.up
    create_table :lojas_tipo_lojas, :id => false do |t|
      t.references :loja
      t.references :tipo_loja
    end
  end

  def self.down
    drop_table :lojas_tipo_lojas
  end
end
