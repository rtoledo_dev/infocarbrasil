class CreateCidades < ActiveRecord::Migration
  def self.up
    create_table :cidades do |t|
      t.references :estado
      t.string :nome
    end
  end

  def self.down
    drop_table :cidades
  end
end
