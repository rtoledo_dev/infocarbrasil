class CreateAnuncios < ActiveRecord::Migration
  def self.up
    create_table :anuncios do |t|
      t.references :anunciante
      t.references :plano
      t.text :descricao
      t.string :tipo
      t.date :expira_em
      t.boolean :status, :default => true

      t.timestamps
    end
  end

  def self.down
    drop_table :anuncios
  end
end
