class CreateFotoNoticias < ActiveRecord::Migration
  def self.up
    create_table :foto_noticias do |t|
      t.references :noticia
      t.string :arquivo
      t.string :descricao
      t.boolean :principal
    end
  end

  def self.down
    drop_table :foto_noticias
  end
end
