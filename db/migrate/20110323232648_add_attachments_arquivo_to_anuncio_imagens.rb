class AddAttachmentsArquivoToAnuncioImagens < ActiveRecord::Migration
  def self.up
    add_column :anuncio_imagens, :arquivo_file_name, :string
    add_column :anuncio_imagens, :arquivo_content_type, :string
    add_column :anuncio_imagens, :arquivo_file_size, :integer
  end

  def self.down
    remove_column :anuncio_imagens, :arquivo_file_name
    remove_column :anuncio_imagens, :arquivo_content_type
    remove_column :anuncio_imagens, :arquivo_file_size
  end
end
