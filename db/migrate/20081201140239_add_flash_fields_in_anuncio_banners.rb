class AddFlashFieldsInAnuncioBanners < ActiveRecord::Migration
  def self.up
    change_table :anuncio_banners do |t|
      t.integer :width, :limit => 4, :null => true, :defaul => nil
      t.integer :height, :limit => 4, :null => true, :defaul => nil
      t.string :wmode, :null => true, :defaul => nil
      t.remove :codigo_flash
    end
  end

  def self.down
    change_table :anuncio_banners do |t|
      t.remove :width
      t.remove :height
      t.remove :wmode
      t.text :codigo_flash
    end
  end
end
