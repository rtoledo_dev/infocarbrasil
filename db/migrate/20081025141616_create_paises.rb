class CreatePaises < ActiveRecord::Migration
  def self.up
    create_table :paises do |t|
      t.string :nome
      t.boolean :ativo
    end
  end

  def self.down
    drop_table :paises
  end
end
