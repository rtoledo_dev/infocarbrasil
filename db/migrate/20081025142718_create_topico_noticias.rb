class CreateTopicoNoticias < ActiveRecord::Migration
  def self.up
    create_table :topico_noticias do |t|
      t.string :descricao

      t.timestamps
    end
  end

  def self.down
    drop_table :topico_noticias
  end
end
