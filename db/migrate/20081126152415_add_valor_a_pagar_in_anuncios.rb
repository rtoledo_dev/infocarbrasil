class AddValorAPagarInAnuncios < ActiveRecord::Migration
  def self.up
    change_table(:anuncios) do |t|
      t.decimal :valor_a_pagar, :null => true, :default => nil
    end
  end

  def self.down
    remove_column :anuncios, :valor_a_pagar
  end
end
