class RemoverArquivos < ActiveRecord::Migration
  def self.up
    remove_column :anuncio_banners, :arquivo
    remove_column :anuncio_imagens, :arquivo
    remove_column :banners,         :arquivo
    remove_column :foto_lojas,      :arquivo
    remove_column :foto_noticias,   :arquivo
  end

  def self.down
  end
end
