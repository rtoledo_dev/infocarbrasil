class CreateAnuncioImagens < ActiveRecord::Migration
  def self.up
    create_table :anuncio_imagens do |t|
      t.references :anuncio
      t.string :descricao
      t.string :arquivo

      t.timestamps
    end
    
    
    change_table(:anuncios) do |t|
      t.references :anuncio_imagem
    end
  end

  def self.down
    change_table(:anuncios) do |t|
      t.remove :anuncio_imagem_id
    end
    
    drop_table :anuncio_imagens
  end
end
