class CreateTipoVeiculos < ActiveRecord::Migration
  def self.up
    create_table :tipo_veiculos do |t|
      t.string :descricao
    end
  end

  def self.down
    drop_table :tipo_veiculos
  end
end
