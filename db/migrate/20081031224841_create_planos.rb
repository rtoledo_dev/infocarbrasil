class CreatePlanos < ActiveRecord::Migration
  def self.up
    create_table :planos do |t|
      t.integer :vigencia
      t.string :descricao
      t.decimal :valor
      t.string :tipo
      t.boolean :destaque

      t.timestamps
    end
  end

  def self.down
    drop_table :planos
  end
end